# Detector: detección de fallas en Erlang

## El consumer

Para realizar los experimentos que se proponen en el proyecto, en primer lugar de debe desarrollar el modulo consumer.
Las funciones requeridas son las siguientes:

``` erlang
start(Producer) ->
  Consumer = spawn(fun() -> init(Producer) end),
  register(consumer, Consumer).
```
Toma un identificador de proceso de un producer como argumento,además de crear un nuevo proceso y registrarlo con el nombre consumer.

``` erlang
init(Producer) ->
  Monitor = monitor(process, Producer),
  Producer ! {hello, self()},
  consumer(0, Monitor).
```
En la función init/1 arrancamos un monitor para el producer, y a este último le enviamos el mensaje {hello, Consumer}. 'hello' para que el producer empiece a enviar mensajes de ping, y 'self()' como el identificador del proceso para recibir los mensajes.

``` erlang
consumer(N, Monitor) ->
  receive 
    {ping, C} -> 
        if 
          C == N ->
            io:format("Contador ~w~n", [N]);
          C >= N ->
          io:format("Warning ~n")
        end,
        consumer(N+1, Monitor);
    {'DOWN', Monitor, process, Object, Info} ->
        io:format("~w died; ~w~n", [Object, Info]),
        consumer(N, Monitor);
    bye ->
      io:format("termine~n"), 
      ok;
    stop ->
      ok
    end.
```

La función consumer/2 espera recibir una secuencia de mensajes ping y revisa que el mensaje contenga el valor esperado. Si es correcto debe imprime el número en
la pantalla y continua. Si recibe un número más alto debe imprime un warning antes de continuar.
Si el producer tiene un crash o si termina, la funcion consumer/2 espera recibir el mensaje  {'DOWN', Monitor, process, Object, Info}.
Si recibe el mensaje stop o bye, el proceso termina.

## Un experimento en dos nodos

## En el mismo host

### ¿Qué mensaje se da como razón cuando el nodo es terminado? ¿Por qué?
Cuando el nodo es terminado da como razón noconnection, porque, según la documentación de Erlang, es la excepción que
se produce cuando un link o un monitor a un proceso remoto es roto por conexión cortada.

![Respuesta_matar_nodo](assets/respuesta%20matar%20nodo.png)

## Un experimento distribuido

-Ejecutemos cada uno de los
nodos en diferentes computadoras. En principio todo debería ser normal y debe-
ríamos ser capaces de provocar un crash en el proceso. ¿Qué sucede si matamos
el nodo Erlang en el producer?

Para estos experimentos decidimos crear una maquina virtual con el sistema operativo Ubuntu.En esta vm levantaremos el proceso erlang de producer.

El mensaje que nos aparece al matar el nodo  del producer en una maquina aparte es 'no conection'. Es el mismo que nos aparece cuando matamos el nodo del producer corriendo en el mismo host (el experimento anterior).

![Respuesta_matar_nodo](assets/Captura%20de%20pantalla%20de%202022-04-19%2014-44-15.png)



-Ahora probemos desconectar el cable de red de la máquina corriendo el
producer y volvamos a enchufarlo despues de unos segundos. ¿Qué pasa? Des-
conectemos el cable por períodos mas largos. ¿Qué pasa ahora?

Al desconectar el cable de red de la máquina corriendo el
producer, el consumer queda esperando mensajes sin recibir algún mensaje de error. Al volver a conectar el cable de red de la maquina que corre el producer, el consumer recibe instantaneamente todos los mensajes que el producer deberia haber enviado si no se hubiese desconectado el cable de red. Estos mensajes conservan el orden, ya que el contador muestra como se incrementa de a una unidad.

![Respuesta_matar_nodo](assets/VirtualBox_desconectar_cable_de_red.png)

![Respuesta_matar_nodo](assets/producer-perdiendo-conexion.png)


### ¿Qué significa haber recibido un mensaje ’DOWN’? ¿Cuándo debemos confiar en el?
Si recibimos el mensaje ’DOWN’, significa que el proceso el monitor vigila ha sido terminado, o nunca existió ese proceso en primer lugar.
Confiamos en él cuando necesitamos reaccionar frente a esos problemas, ya sea aplicando demonitor/2 o reiniciando
de nuevo el proceso.

### ¿Se recibieron mensajes fuera de orden, aun sin haber recibido un mensaje ’DOWN’? ¿Qué dice el manual acerca de las garantías de envíos de mensajes?
No se recibieron mensajes fuera de orden, si no que al reconectar el cable de red llegan todos a la vez y en orden.
Sobre la garantía de envíos de mensajes, Erlang asegura que mientras que se haga entre dos procesos, el orden relativo de envío se mantendrá, pero no puede
asegurar que el proceso destino o receptor ha terminado o no está disponible. Los monitores pueden ayudar en esos casos, pero nada más.
 
