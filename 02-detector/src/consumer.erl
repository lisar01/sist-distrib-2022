-module(consumer).

-export([start/1, stop/0]).

%% *Al inicializar deberá enviar un mensaje hello al producer antes de
%% entrar en su estado recursivo con un valor esperado puesto en 0.
%%
%% *Deberá recibir una secuencia de mensajes ping y revisar que el
%% mensaje contenga el valor esperado. Si es correcto debe imprimir
%% el número en la pantalla y continuar. Si recibe un número más alto
%% debe imprimir un warning antes de continuar. En ambos casos el valor
%% esperado siguiente es uno más que el valor recibido.
%%
%% *Si el proceso recibe un mensaje bye (enviado por el producer) o un
%% mensaje stop (enviado al llamar stop/0 el proceso debe terminar.
%%
%% Además de tener un parámetro adicional, un mensaje más es agregado a la
%% definición recursiva.
%%    {'DOWN', Monitor, process, Object, Info} ->
%%      io:format("~w died; ~w~n", [Object, Info]),
%%      consumer(N, Monitor);


start(Producer) ->
  Consumer = spawn(fun() -> init(Producer) end),
  register(consumer, Consumer).


init(Producer) ->
  Monitor = monitor(process, Producer),
  Producer ! {hello, self()},
  consumer(0, Monitor).

consumer(N, Monitor) ->
  receive 
    {ping, C} -> 
        if 
          C == N ->
            io:format("Contador ~w~n", [N]);
          C >= N ->
          io:format("Warning ~n")
        end,
        consumer(N+1, Monitor);
    {'DOWN', Monitor, process, Object, Info} ->
        io:format("~w died; ~w~n", [Object, Info]),
        consumer(N, Monitor);
    bye ->
      io:format("termine~n"), 
      ok;
    stop ->
      ok
    end.
  


stop() ->
  consumer ! stop.
