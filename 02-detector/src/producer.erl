-module(producer).
-export([start/1, stop/0, crash/0]).

%% Creará un nuevo proceso y lo registrará bajo el nombre producer. Recibe un parámetro llamado Delay,
%% que sera la cantidad de milisegundos entre mensajes. La función stop/0 simplemente
%% enviará un mensaje de stop al proceso registrado bajo el nombre producer
start(Delay) ->
  Producer = spawn(fun() -> init(Delay) end),
  register(producer, Producer).

%% En la inicialización del producer, esperaremos a que un consumer nos envíe un mensaje. Un consumer enviará
%% un mensaje hello, donde Consumer es el identificador del proceso que debe recibir los mensajes.
init(Delay) ->
  receive
    {hello, Consumer} ->
      producer(Consumer, 0, Delay);
    stop ->
      ok
  end.

%% El proceso está luego implementado usando la construcción after que nos permite esperar por un mensaje un
%% cierto tiempo antes de continuar. Si no es recibido ningún mensaje de stop enviamos un mensaje ping al consumer.
producer(Consumer, N, Delay) ->
  receive
    stop ->
      Consumer ! bye;
    crash ->
      42/0 %% this will give you a warning, but it is ok
  after Delay ->
    Consumer ! {ping, N},
    producer(Consumer, N+1, Delay)
  end.


stop() ->
  producer ! stop.

crash() ->
  producer ! crash.
