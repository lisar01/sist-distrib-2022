-module(client).

-export([start/1, open/1, read/2, wait_read/0, write/3, commit/1, wait_commit/0]).

start(Server) ->
  spawn(fun() -> init(Server, 0, 0) end).


%% Para cada operacion hecha me guardo si el commit fue exitoso o no
init(Server, Successful_T, Failed_T) ->
  receive
    {A, B, C, op1} ->
      {_, New_S, New_F} = client_ops:operacion1(Server, A, B, C),
      init(Server,Successful_T + New_S, Failed_T + New_F);
    {A, B, C, op2} ->
      {_, New_S, New_F} = client_ops:operacion2(Server, A, B, C),
      init(Server,Successful_T + New_S, Failed_T + New_F);
    {A, B, C, D, op3} ->
      {_, New_S, New_F} = client_ops:operacion3(Server, A, B, C, D),
      init(Server,Successful_T + New_S, Failed_T + New_F);
    {A, B, C, D, E, F, op4} ->
      {_, New_S, New_F} = client_ops:operacion4(Server, A, B, C, D, E, F),
      init(Server,Successful_T + New_S, Failed_T + New_F);
    {A, B, C, D, E, F, op5} ->
      {_, New_S, New_F} = client_ops:operacion5(Server, A, B, C, D, E, F),
      init(Server,Successful_T + New_S, Failed_T + New_F);
    {stop, Client} ->
      Client ! {state, Successful_T, Failed_T}
  end.


open(Server) ->
  Server ! {open, self()},
  receive
    {transaction, Validator, Store} ->
      H = handler:start(self(), Validator, Store),
      %io:format("Conexion establecida, Handler: ~w~n", [H]),
      H
  end.

read(Handler, N) ->
  Ref = make_ref(),
  %io:format("Read, N: ~w, Ref: ~w, Handler: ~w~n", [N, Ref, Handler]),
  Handler ! {read, Ref, N},
  Ref.

wait_read() ->
  receive
    {read, Ref, Value} ->
      %io:format("Read, Recibido Value: ~w, Ref: ~w~n", [Value, Ref]),
      {Ref, Value}
  end.

write(Handler, N, Value) ->
  %io:format("Write, N: ~w, Value: ~w, Handler: ~w~n", [N, Value, Handler]),
  Handler ! {write, N, Value}.

commit(Handler) ->
  Handler ! {commit, make_ref()}.

wait_commit() ->
  receive
    {commit, Ref, ok} ->
      %io:format("Commit exitoso: ~w~n", [Ref]),
      {Ref, 1, 0};
    {commit, Ref, abort} ->
      %io:format("Commit fallo: ~w~n", [Ref]),
      {Ref, 0, 1}
  end.
