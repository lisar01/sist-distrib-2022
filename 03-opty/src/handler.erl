-module(handler).
-export([start/3]).

start(Client, Validator, Store) ->
  spawn_link(fun() -> init(Client, Validator, Store) end).

init(Client, Validator, Store) ->
  handler(Client, Validator, Store, [], []).

%
% escrituras: [{indice de la entrada, pid entrada, nuevo valor},...]
% lecturas: [{pid_entrada, timestamp}]
%
handler(Client, Validator, Store, Reads, Writes) ->
  receive
%%  read, Ref, N: un pedido de lectura de un cliente contiene una
%%  referencia que debemos usar en el mensaje de respuesta. Un entero
%%  N que el índice de la entrada en el store. El handler debe primero
%%  mirar el conjunto de escrituras y ver si la entrada N ha sido
%%  escrita. Si no encontramos una operación de escritura se envía un
%%  mensaje al proceso de la N-ésima entrada del store. Esta entrada va
%%  a contestar al handler dado que debemos registrar el tiempo de
%%  lectura.
    {read, Ref, N} ->
      case lists:keysearch(N, 1, Writes) of
        {value, {N, Pid_Entry, Value}} ->
          Pid_Entry ! {time, Ref, Value, self()};
        false ->
          Entry = lookup(N, Store),
          Entry ! {read, Ref, self()}
      end,
      handler(Client, Validator, Store, Reads, Writes);
%%   Ref, Entry, Value, Time: una respuesta de una entrada debe ser
%%   reenviada al cliente. La entrada y el tiempo se guarda en el
%%   conjunto de lecturas del handler. La respuesta al cliente es
%%   Ref, Value.
    {Ref, Entry, Value, Time} ->
      Added = [{Entry, Time}|Reads],
      Client ! {read, Ref, Value},
      handler(Client, Validator, Store, Added, Writes);
%%   write, N, Value: un mensaje de escritura de un cliente. El entero
%%   N es el índice de la entrada en el store y Value el valor. La
%%   entrada con índice N y el valor son guardados en el conjunto de
%%   escrituras del handler.
    {write, N, Value} ->
      Entry = lookup(N, Store),
      Added = [{N, Entry, Value}|Writes],
      handler(Client, Validator, Store, Reads, Added);
    {commit, Ref} ->
      Validator ! {validate, Ref, Reads, Writes, Client};
    abort -> ok
  end.


lookup(I, Store) ->
  try store:lookup(I, Store) of
    Entry -> Entry
  catch
    error:_ -> {error, "Indice de entrada no existente"}
  end.