-module(client_ops).


-export([operacion1/4, operacion2/4, operacion3/5, operacion4/7, operacion5/7]).

operacion1(Server, A, B, C) ->
  %% Obtener transaccion
  Handler = client:open(Server),

  %% leer Entry 1
  client:read(Handler, A),
  %% leer Entry 2
  client:read(Handler, B),

  %% [V1, V2] = receive_values([Ref1, Ref2]),
  {_, V1} = client:wait_read(),
  {_, V2} = client:wait_read(),

  %% sumar los valores
  R = V1 + V2,

  %% escribirloa en Entry 3
  client:write(Handler, C, R),

  %% commit
  client:commit(Handler),
  client:wait_commit().

operacion2(Server, A, B, C) ->
  %% Obtener transaccion
  Handler = client:open(Server),

  %% leer Entry 1
  client:read(Handler, A),
  %% leer Entry 2
  client:read(Handler, B),

  %% [V1, V2] = receive_values([Ref1, Ref2]),
  {_, V1} = client:wait_read(),
  {_, V2} = client:wait_read(),

  %% sumar los valores
  R = V1 - V2,

  %% escribirloa en Entry 3
  client:write(Handler, C, R),

  %% commit
  client:commit(Handler),
  client:wait_commit().

operacion3(Server, A, B, C, D) ->
  %% Obtener transaccion
  Handler = client:open(Server),

  %% leer Entry 1
  client:read(Handler, A),
  %% leer Entry 2
  client:read(Handler, B),
  %% leer Entry 3
  client:read(Handler, C),

  %% [V1, V2] = receive_values([Ref1, Ref2]),
  {_, V1} = client:wait_read(),
  {_, V2} = client:wait_read(),
  {_, V3} = client:wait_read(),

  %% operar los valores
  Rp = V1 - V2,
  Rt = Rp + V3,

  %% escribirloa en Entry 4
  client:write(Handler, D, Rt),

  %% commit
  client:commit(Handler),
  client:wait_commit().

operacion4(Server, A, B, C, D, E, F) ->
  Handler = client:open(Server),

  client:write(Handler, A, D),
  client:write(Handler, B, E),
  client:write(Handler, C, F),

  client:commit(Handler),
  client:wait_commit().


operacion5(Server, A, B, C, D, E, F) ->
  Handler = client:open(Server),

  client:write(Handler, A, F),

  client:read(Handler, B),
  {_, VB} = client:wait_read(),

  client:read(Handler, C),
  {_, VC} = client:wait_read(),

  client:write(Handler, D, VB + VC + F),
  client:write(Handler, E, F),

  client:commit(Handler),
  client:wait_commit().

%%receive_values(List, 0) ->
%%  List;
%%receive_values(List, N) ->
%%  receive
%%    {Ref, Value} ->
%%      replace(Ref, Value, List),
%%      receive_values(List, N - 1)
%%  end.
%%
%%replace(Ref, Value,  Ls) ->
%%  lists:map(fun (E) ->
%%    if E == Ref -> Value;
%%      true -> Ref
%%    end end,
%%    Ls).