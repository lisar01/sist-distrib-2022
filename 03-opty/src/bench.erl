-module(bench).

-export([start/2]).


start(N_Entries, N_Clients) ->
  S = server:start(N_Entries),
  Counter = spawn(fun() -> counter(N_Clients,0,0) end),
  [random_client(N_Entries, S, Counter) || _ <- lists:seq(1, N_Clients)],
  S.


%% Cuenta cuantos commits fallidos y exitosos realizaron todos los clientes
%% una vez que le enviaron todos ellos, termina e imprime el resultado
counter(0,S,F) -> io:format("Exitos: ~w Fallas: ~w~n", [S,F]);
counter(N_Clients, S, F) ->
  receive
    {state, C_S, C_F} -> counter(N_Clients - 1, S + C_S, F + C_F)
  end.


%% Crea un cliente que realiza una aperacion random con indices y valores random
random_client(N_Entries, S, Counter) ->
  %% lista de posibles operaciones que puede hacer un cliente con la forma
  %% {atomo de op, cantidad de indices que usa la funcion, valores que usa la funcion}
  Ops = [{op1,3,0}, {op2,3,0}, {op3,4,0},{op4,3,3},{op5,5,1}],

  %% elijo una operacion random
  Op = lists:nth(rand:uniform(5), Ops),

  Random_indexes = generate_n_numbers_less_than(element(2, Op), N_Entries),
  Random_values = generate_n_numbers_less_than(element(3, Op), 30),

  %% la convierto en la forma {i1,i2,..i_n,v1,v2,...v_m,atomo op}
  Random_op = list_to_tuple(lists:merge([Random_values, Random_indexes, [element(1,Op)]])),
  %io:format("Random op: ~w ~n", [Random_op]),

  %% creo un cliente que realiza una operacion aleatoria, se lo para y le paso el Counter para
  %% que le envia su state (cuantos commits exitosos y fallidos hizo)
  C = client:start(S),
  C ! Random_op,
  C ! {stop, Counter}.


%% Devuelve una lista de n numeros menores que Less_than
generate_n_numbers_less_than(0, _) -> [];
generate_n_numbers_less_than(N, Less_Than) ->
  [rand:uniform(Less_Than) | generate_n_numbers_less_than(N-1, Less_Than)].

