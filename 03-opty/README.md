## Opty: control concurrencia optimista

### Implementacion de Opty

En primer lugar los espacios en lo que habia que completar codigo pertenecian a los modulos `handler`, `server` y `validator`.
Luego era necesaria una implementación de un modulo `cliente` y un modulo de pruebas para analizar el metodo de control de concurrencia optimista. Este modulo lo denominamos `bench`.

<br>

### Handler

En el modulo handler quedaba por completar el accionar del handler de transacciones cuando recibia distintos mensajes del cliente o de las entradas. Dependiendo del mensaje que recibia, el handler debia enviar un mensaje a una entrada, enviar un mensaje al cliente, enviar un mensaje al validator, actualizar la lista de lecturas o actualizar la lista de escrituras.
La función handler/5 quedaría de la siguiente forma:

```erlang
handler(Client, Validator, Store, Reads, Writes) ->
  receive
    {read, Ref, N} ->
      case lists:keysearch(N, 1, Writes) of
        {value, {N, Pid_Entry, Value}} ->
          Pid_Entry ! {time, Ref, Value, self()};
        false ->
          Entry = lookup(N, Store),
          Entry ! {read, Ref, self()}
      end,
      handler(Client, Validator, Store, Reads, Writes);
    {Ref, Entry, Value, Time} ->
      Added = [{Entry, Time}|Reads],
      Client ! {read, Ref, Value},
      handler(Client, Validator, Store, Added, Writes);
    {write, N, Value} ->
      Entry = lookup(N, Store),
      Added = [{N, Entry, Value}|Writes],
      handler(Client, Validator, Store, Reads, Added);
    {commit, Ref} ->
      Validator ! {validate, Ref, Reads, Writes, Client};
    abort -> ok
  end.

```

<br>

### Server

En el modulo server quedaba por completar la conexión de un cliente con el server.
Cuando el server reciba el mensaje {open, Client}, este debia responderle al cliente otorgandole un validotor y un store, para el cliente pueda crear un handler de transacciones.
La función server/2 quedaria de la siguiente manera:

```erlang
server(Validator, Store) ->
  receive
    {open, Client} ->
      Client ! {transaction, Validator, Store},
      server(Validator, Store);
    stop ->
      store:stop(Store)
  end.
```
<br>

### Validator

En el modulo validator quedaba por completa como este actualiza el store con las operaciones de escritura pendientes.
La función update/1 quedaría de la siguiente forma:

```erlang
update(Writes) ->
  lists:foreach(
    fun({_, Entry, New}) ->
      Entry ! {write, New}
    end,
    Writes).
```
<br>

### Client

El modulo client habia que desarrollarlo completamente. Este se encarga de establecer una conexión con el server, crear un handler de transacciónes y mediante este, realizar diferentes operaciónes, ya sea de lectura o escritura.
<br>
 Se optó por recrear diferentes escenarios de clientes. Más especificamente crear distintas simulaciónes de transacciones que un cliente usual puede realizar. Estas operaciones solo necesitan como parametro los indices de las entradas en las que van a realizar las lecturas o escrituras. Por ejemplo, la función operacion1/3 toma como parametro 3 indices. El primer indice de entrada lo leeria junto con el segundo, para sumar sus valores y escribir el resultado en el tercer indice de entrada.

### Benchs
Para poder testear la implementación de opty de diferentes formas, optamos por tener dos módulos `bench.erl`. Bench 1
se encuentra en la rama `main` y Bench 2 en la rama `opty_alternativo`.

#### Bench 1
Se encarga de correr el servidor con una cantidad n de entradas y m cantidad de clientes que realizan transacciones en entradas aleatorias. Al finalizar de correr todos los clientes, se
imprime en consola la cantidad de commits fallidos y exitosos. Para hacer esto cuenta con 3 funciones principales:

- `start`: Levanta un servidor con n entradas, el contador de commits y m clientes aleatorios.
- `random_client`: Elije una operación para que haga el cliente con parámetros aleatorios. Levanta el cliente;
  le envía la operación y los parámetros; y lo para, pasándole el PID del contador para enviarle su estado.
- `counter`: Cuenta la cantidad de commits fallidos y exitosos. Cada cliente al finalizar le envía esa información,
  la guarda y una vez terminado todos los clientes, la imprime.

``` erlang
9> bench:start(10,20).                                                                          
<0.120.0>
10> Exitos: 7 Fallas: 13
```

#### Bench 2

Al igual que el bench 1, este bench se encarga de correr el servidor con una cantidad n de entradas y m cantidad de clientes. Sin embargo, se inicializan 3 tipos diferentes de clientes que tienen sus transacciones definidas de antemano. Optamos por que un cliente realize acciones usuales como leer 2 entradas, sumarlas, restarlas o ambas, y escribir en otra entrada. A diferencia del bench 1, en esta implementación lo unico random son las entradas en las que los clientes operan. De esta manera, por cada cliente que se desee crear por parametro en la función <bench:start(50,5)>, se crean 3 que realizan transacciones diferentes, por lo que en ese ejemplo se tendrían 50 entradas y 15 clientes.

<br>

### ¿Performa? ¿Cuáles son las limitaciones en el número de transacciones concurrentes y la tasa de éxito? Esto por supuesto depende del tamaño del store, cuántas operaciones de write hace cada transacción, cuanto tiempo tenemos entre las instrucciones de read y el commit final. ¿Algo más?

<br>

Existen limitaciones en el número de transacciones concurrentes. Al utilizar un control de concurrencia optimista existe una relación entre la cantidad de entradas y la cantidad de clientes concurrentes. Los commits exitosos aumentan si las entradas son mayores que los clientes y mientras más grande sea esa diferencia, mayores son los commits exitosos en los clientes concurrentes. <br>
En nuestras pruebas no se realizan mas de 3 instrucciones de read por cliente. Además de una operación de write. Con estas implementaciones las tazas de exito se ven reflejadas con lo que se comento arriba. Estos resultados corresponden al bench 2, aunque con ambos bench llegamos a las mismas conclusiones.

- 100 entradas y 30 clientes:
``` erlang
11> bench:start(100,10).                                                                          
taza de exito en porcentaje: 73.3
```

- 300 entradas y 30 clientes:
``` erlang
12> bench:start(300,10).                                                                          
taza de exito en porcentaje: 93
```

- 3000 entradas y 30 clientes:
``` erlang
13> bench:start(3000,10).                                                                          
taza de exito en porcentaje: 100
```

Reciprocamente, mientras mas clientes concurrentes con un store limitado, los commits que fallan aumentan.

- 100 entradas y 90 clientes:
``` erlang
8> bench:start(100,30).                                                                          
taza de exito en porcentaje: 52.2
```

- 90 entradas y 90 clientes:

``` erlang
9> bench:start(90,30).                                                                          
taza de exito en porcentaje: 48.8
```

- 60 entradas y 90 clientes:

``` erlang
12> bench:start(60,30).                                                                          
taza de exito en porcentaje: 38.8
```

Vale la pena comentar que si los clientes que hacen transacciones concurrentes escriben todos en las mismas entradas, los commits que fallan son muchisimos mas.

<br>

### ¿Es realista la implementación del store que tenemos?
<br>
La implementación para nosotros no sería realista en otros lenguajes de programación, pero en Erlang donde crear y
terminar un proceso es tan fácil y rápido, es creíble ese store.

<br>

### Independientemente del handler de transacciones, ¿qué rápido podemos operar sobre él store?

<br>

La rapidez del store depende que tan ocupados están los procesos entradas, pero tendría que tener muchisimos mensajes
en cola un proceso de entrada para que la tardanza se note.

<br>

### ¿Qué sucede si hacemos esto en una red de Erlang distribuida, qué es lo que se copia cuando el handler de transacciones arranca? ¿Dónde corre el handler?

<br>

A la hora de utilizar Opty en una red de Erlang distribuida, nosotros optamos por levantar el server en una maquina y que otra maquina sirva como cliente para realizar una transaccion predefinida, por lo que el handler de transacciones correria en la maquina del cliente.
En primer lugar, conectamos los nodos con las respectivas direcciones IP. La unica diferencia a la hora de utilizar Opty en una red distribuida es que, cuando se quiera enviar el server como parametro, ya no se utiliza `server` como nombre registrado del proceso donde corre el server, sino `{server, server@ip_maquina_donde_corre_el_server}`. Asi, por ejemplo la llamada de la función start/1 del cliente quedaria de la siguiente forma:

``` erlang
2> client:start({server, 'server@192.168.1.100'}).
```

<br>

### ¿Cuáles son los pros y contras de la estrategia de implementación?

<br>

Las ventajas de la implementación de opty son la rapidez, las transacciones con los clientes se hacen en un toque;
el manejo de la transacción (handler) depende del cliente, sacando responsabilidades al server; un porcentaje alto
de commits exitosos si el server tiene muchas entradas y pocos clientes.

``` erlang
16> bench:start(200,100). 
<0.516.0>
17> Exitos: 83 Fallas: 17
```

Las desventajas son: el server es inutilizable si se cae un proceso entrada; un porcentaje alto de commits fallidos
si el servidor tiene pocas entradas pero muchos clientes.

``` erlang
12> bench:start(10,100).
<0.176.0> 
12> Exitos: 36 Fallas: 64
```
