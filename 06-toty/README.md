## Toty: multicast con orden total

### 2.3 Multicast Basico

Para realizar el experimento que implementa multicast basico, primero se desarrollaron los modulos:

- `worker`: El proceso que en intervalos aleatorios realiza un pedido de enviar un mensaje multicast a un grupo con otros workers.
- `multicaster`: Es el proceso que implementa multicast basico. Este esta comunicacion con un worker (su master) y con otros procesos multicaster pares, los cuales pertenecen a otros workers.
- `gui`: El proceso que instancia una interfaz grafica y colorea una ventana dependiendo de los mensajes que se envian los workers. Esta implementacion fue desarrollada utilizando la libreria WX.
- `toty`: Este modulo se engarga de instanciar las diferentes partes del experimento y correrlo.


Una vez que todo esta en funcionamiento, se ve como inician las ventanas de los respectivos workers. Una vez que intercambian mensajes, los colores de estas ventanas van modificandose. Dependiendo de los valores de `Sleep`(el tiempo de cuanto deben esperar los workers hasta el envío del siguiente mensaje) y `Jitter` (tiempo de espera cuando el proceso arranca), los colores cambian mas o menos rapido.

Primeros segundos de una ejecucion.
![Consolas con_rudy secuencial](assets/VentanasWorkers1.png)
<br> Cambios de colores despues de iniciar la ejecucion.
![Consolas con_rudy secuencial](assets/VentanasWorkers2.png)
<br> Luego de varios segundos.
![Consolas con_rudy secuencial](assets/VentanasWorkes3.png)

-Analizar que pasa si tenemos un sistema que se basa en una entrega con
orden total pero esto no fue claramente establecido. ¿Si la congestión es baja y
no tenemos retrasos en la red, cuánto tiempo tarda antes de que los mensajes
se entreguen fuera de orden? ¿Cuán difícil es hacer debug del sistema y darnos
cuenta que es lo que está mal?

Si la congestion es baja y no tenemos retrasos en la red (en nuestro caso se traduciria a valores bajos de sleep y jitter), los mensajes se desordenarian de una manera menos instantanea ya que los workers y los procesos multicaster tendrian menos oportunidades de colision, todo se entregaria de la manera mas rapida y secuencial posible, simulando un orden total, pero es algo que bajo esta implementacion no se puede aseguarar 100%.
Hacer debug y darnos cuenta de lo que esta mal es dificil, ya que lo que nos importa es el orden de entrega de los mensajes. Esto significa que el orden temporal para a un segundo plano y deja la posibilidad de que los mensajes se entreguen en diferente tiempo, pero en un mismo orden. Bajo este experimento, esto se traduciria a que cada una de las ventanas este de un color diferente en un momento especifico, pero puede que todas esten cambiando de color en una misma secuencia, primero un color y luego otro, pero en distinto orden temporar. Por este motivo a alguien que observa los resultados le resultaria muy dificil saber si los mensajes se estan entregando en el mismo orden, o si estan desordenados.

### Multicast con Orden Total
En este caso se pide implementar un servicio de multicast con orden total usando un algoritmo con un mecanismo descentralizado (ISIS).
La idea principal es que los nodos consensúen el orden de los mensajes, proponiendo cada uno de ellos un orden potencial (la propuesta).
Pudimos hacerlo funcionar.

La dificultad del trabajo era mayor que los otros porque venía con menos código ya hecho, y más para hacer nosotros. Además, nos costó al
principio entender cómo funciona el algoritmo y las funciones ya provistas en el módulo. Tardamos en entender para qué necesitaba la
secuencia el `Id`, los nombres de la tupla que se guarda el `Cast`, como se calcula el nuevo `Next` y los mensajes enviables, luego de llegar
un mensaje consensuado.

Para implementar la nueva versión de multicast, creamos un nuevo módulo `multicaster2`. Los módulos `worker`, `gui` y `toty` (el manager en la consigna),
no fueron necesarios modificar. Solo a `worker` y `toty`, se le agregaron unos prints para chequear si realmente hay orden total. Cada `worker` guarda
los colores cambiados y al hacer stop en `toty`, el `worker` le da sus colores a `toty`, controlando que los colores estén en el mismo orden. Agregamos
esa forma, porque era difícil recordar los colores anteriores en la interfaz.

Cuando hacemos stop de un `worker`, le agregamos que entre en un estado `stopping` para que espere un tiempo los mensajes de su `multicaster`.
Pero no lo pudimos hacer andar, algunos mensajes de un multicaster faltaban llegar.

Para implementar la cola de mensajes de un multicast, creamos otro módulo, que utiliza la librería orddict de diccionarios ordenados, para
facilitarnos ordenar según la secuencia (`{N, Id}`). Guardamos como `proposed` los mensajes propuestos y `agreed`, los consensuados.

Se respetó la plantilla dada, completando los signos de pregunta. El único diferente es la función `agreed`, que toma un solo parámetro,
la cola actualizada, en vez de dos. Asumimos que el segundo parámetro es el `N` de la `Seq`, pero no fue necesario, con sacar los primeros
`agreed` de la lista hasta encontrar un `proposed` alcanza.

#### Tenemos muchos mensajes en el sistema, ¿cuántos mensajes podemos hacer multicast por segundo y cómo depende esto del número de workers?
Hay muchos mensajes en el sistema, por cada `worker` que envía un mensaje para hacer multicasting, se envían 4*N (cantidad de workers) mensajes
(cuando se propone, cuando le responden,  cuando envío el consensuado y de mensaje al `worker`). Entonces, por segundo, van a hacerse muchos aunque
también depende del tiempo `Sleep` y `Jitter` definidos.

#### Construir una red distribuida, ¿cuán grande puede ser antes de que empiece a fallar?
Si construyeramos una red distribuida, como tenemos definido de forma estática los workers y los multicasters, tendríamos que modificar `toty`
para que reciba workers que quieran suscribirse. Al recibir la petición, `toty` le respondería con los multicasters ya participando, el `worker`
crearía el `multicast` dando sus pares. Le informaría de esto a `toty` y este le avisa que hay un nuevo `multicaster` a los demás.
Como los multicasters pueden ahora estar en lugares alejados, pueden llegar a pasar diferentes cosas que hagan fallar al sistema. Problemas
de conexión o que algún `worker`, `multicaster` o `toty` falle. Haciendo que cuanto mas workers haya, más chances que se pierda el orden total
o se cuelguen los multicasters esperando el llamado de algún nodo caído.
