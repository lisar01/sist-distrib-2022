-module(multicaster).

-export([start/2]).

start(Jitter, Seed) ->
  spawn(fun() -> init(Jitter, Seed) end).

init(Jitter, Seed) ->
  receive
    {peers, Nodes, Master} ->
      random:seed(Seed, Seed, Seed),
      server(Nodes, Jitter, Master);
    stop ->
      ok
  end.

server(Nodes, Jitter, Master) ->
  receive
    {send, Msg} ->
      request(Nodes, Msg),
      timer:sleep(random:uniform(Jitter)),
      server(Nodes, Jitter, Master);
    {request, Msg} ->
      Master ! {receiving, Msg},
      server(Nodes, Jitter, Master);
    stop ->
      ok
  end.

%enviar a otros multicaster
request(Nodes, Msg) ->
  lists:foreach(fun(N) -> N ! {request, Msg} end, Nodes).