-module(worker).

-export([start/5]).

start(Name, Multicaster, Seed, Sleep, Color) ->
  spawn(fun() -> init(Name, Multicaster, Seed, Sleep, Color) end).

init(Name, Multicaster, Seed, Sleep, Color) ->
  Gui = spawn(gui, init, [Name, Color]),
  random:seed(Seed, Seed, Seed),
  send(Name, Multicaster, Sleep, Color, Gui, []).

send(Name, Multicaster, Sleep, Color, Gui, Colors) ->
  Multicaster ! {send, {Name, random:uniform(20)}},
  receiving(Name, Multicaster, Sleep, Color, Gui, Colors).

receiving(Name, Multicaster, Sleep, Color, Gui, Colors) ->
  receive
    {receiving, {Sender, Msg}} ->
      NewColor = updateColor(Color, Msg, Gui),
      %io:format("~s color cambiado: ~w ~n", [Name, NewColor]),
      NewColors = Colors ++ [NewColor],%[NewColor|Colors],
      if Sender =:= Name ->
        timer:sleep(random:uniform(Sleep)),
        send(Name, Multicaster, Sleep, NewColor, Gui, NewColors);
      true ->
        receiving(Name, Multicaster, Sleep, NewColor, Gui, NewColors)
      end;
    {stop, Toty} ->
      Multicaster ! stop,
      stopping(Toty, Name, Multicaster, Sleep, Color, Gui, Colors)
  end.

stopping(Toty, Name, Multicaster, Sleep, Color, Gui, Colors) ->
  receive
    {receiving, {_, Msg}} ->
      NewColor = updateColor(Color, Msg, Gui),
      NewColors = Colors ++ [NewColor],%[NewColor|Colors],
      stopping(Toty, Name, Multicaster, Sleep, NewColor, Gui, NewColors)
  after Sleep ->
    io:format("~s colores cambiados: ~w ~n", [Name, Colors]),
    Gui ! stop,
    Toty ! Colors,
    ok
  end.

updateColor({R, G, B}, Msg, Gui) ->
  NewColor = {G, B, (R + Msg) rem 256},
  Gui ! {color, NewColor},
  NewColor.
