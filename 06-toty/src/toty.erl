-module(toty).

-export([start/3, stop/0]).

start(Multicaster, Sleep, Jitter) ->
    M1 = apply(Multicaster, start, [Jitter, 34]),
    M2 = apply(Multicaster, start, [Jitter, 37]),
    M3 = apply(Multicaster, start, [Jitter, 43]),
    M4 = apply(Multicaster, start, [Jitter, 72]),

    register(w1, worker:start("John", M1, 34, Sleep, {0,0,0})),
    register(w2, worker:start("Ringo",M2, 37, Sleep, {0,0,0})),
    register(w3, worker:start("Paul", M3, 43, Sleep, {0,0,0})),
    register(w4, worker:start("George", M4, 72, Sleep, {0,0,0})),

    M1 ! {peers, [M1, M2, M3, M4], whereis(w1)},
    M2 ! {peers, [M1, M2, M3, M4], whereis(w2)},
    M3 ! {peers, [M1, M2, M3, M4], whereis(w3)},
    M4 ! {peers, [M1, M2, M3, M4], whereis(w4)},

    ok.

stop() ->
  compare(stop(w1), stop(w2), stop(w3), stop(w4), 0).

stop(Name) ->
  case whereis(Name) of
    undefined ->
      ok;
    Pid ->
      Pid ! {stop, self()}
  end,
  receive
    Colors -> Colors
  end.

compare(Cs1, Cs2, Cs3, Cs4, N) when Cs1 =:= [] orelse Cs2 =:= [] orelse Cs3 =:= [] orelse Cs4 =:= [] -> io:format("Hay orden total, ~w colores cambiados ~n", [N]);
compare([C | Cs1], [C | Cs2], [C | Cs3], [C | Cs4], N) -> compare(Cs1, Cs2, Cs3, Cs4, N+1);
compare([C1 | _], [C2 | _], [C3 | _], [C4 | _], N) ->
io:format("Se pierde el orden total en el Color ~w, no coincide: John ~w Ringo ~w Paul ~w George ~w ~n", [N, C1, C2, C3, C4]).