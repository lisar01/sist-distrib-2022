-module(multicaster2).

-export([start/2]).

start(Jitter, Seed) ->
  spawn(fun() -> init(Jitter, Seed) end).

init(Jitter, Seed) ->
  receive
    {peers, Nodes, Master} ->
      random:seed(Seed, Seed, Seed),
      server(Master, 0, Nodes, [], message_queue:new(), Jitter);
    stop ->
      ok
  end.

server(Master, Next, Nodes, Cast, Queue, Jitter) ->
  receive
    {send, Msg} ->
      Ref = make_ref(),
      request(self(), Nodes, Msg, Ref),
      NewCast = cast(Cast, Ref, length(Nodes)),
      server(Master, Next, Nodes, NewCast, Queue, Jitter);

    {request, From, Ref, Msg} ->
      From ! {proposal, Ref, Next},
      NewQueue = insert(Queue, {Next, From}, Ref, Msg),
      NewNext = increment(Next),
      timer:sleep(random:uniform(Jitter)),
      server(Master, NewNext, Nodes, Cast, NewQueue, Jitter);

    {proposal, Ref, Proposal} ->
      case proposal(Cast, Ref, Proposal) of
        {agreed, SeqN, NewCast} ->
          agree(Nodes, Ref, SeqN),
          server(Master, Next, Nodes, NewCast, Queue, Jitter);
        NewCast ->
          server(Master, Next, Nodes, NewCast, Queue, Jitter)
      end;

    {agreed, Ref, SeqN} ->
      UpdatedQueue = update(Queue, Ref, SeqN),
      {Agreed, NewQueue} = agreed(UpdatedQueue),
      deliver(Master, Agreed),
      Next2 = increment(Next, SeqN),
      server(Master, Next2, Nodes, Cast, NewQueue, Jitter);
    stop ->
      ok
  end.

request(Multicaster, Nodes, Msg, Ref) ->
  lists:foreach(fun(N) -> N ! {request, Multicaster, Ref, Msg} end, Nodes).

cast(Cast, Ref, ProposalsLeft) ->
  [{Ref, ProposalsLeft, 0} | Cast].

insert(Queue, Seq, Ref, Msg) ->
  message_queue:insert(Queue, Seq, {proposed, Ref, Msg}).

increment(Next) ->
  Next + 1.

increment(Next, N) ->
  max(Next, N) + 1.

%%Si la propuesta es es la última que estamos esperando
%%entonces hemos encontrado y consensuado el número de secuencia.
proposal(Cast, Ref, Proposal) ->
  {Ref, N, Sofar} = lists:keyfind(Ref, 1, Cast),
  NewTuple = {Ref, N-1, max(Sofar, Proposal)},
  case NewTuple of
    {_, 0, NewSofar} -> 
      NewCast = lists:keydelete(Ref, 1, Cast),
      {agreed, NewSofar, NewCast};
    _ -> lists:keyreplace(Ref, 1, Cast, NewTuple)
  end.

update(Queue, Ref, SeqN) ->
  message_queue:update(Queue, Ref, SeqN).

agreed(UpdatedQueue) ->
  message_queue:agreed(UpdatedQueue).

%Encontrar la referencia en la lista
%Si encontramos {Ref, 1, Sofar} entonces devolvemos {agreed, Proposal, NewCast} eliminando la tupla que continene Ref}
%Si encontramos {Ref, N>1, Sofar} entonces devolvemos NewCast actualizando la tupla {Ref, N-1, Proposal}
agree(Nodes, Ref, Seq) ->
  lists:foreach(fun(N) -> N ! {agreed, Ref, Seq} end, Nodes).

deliver(Master, Agreed) ->
  lists:foreach(fun(Msg) -> Master ! {receiving, Msg} end, Agreed).
