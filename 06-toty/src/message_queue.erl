-module(message_queue).

-export([new/0, insert/3, update/3, agreed/1]).

new() -> orddict:new().

insert(Queue, Key, Value) -> orddict:append(Key, Value, Queue).

update(Queue, Ref1, SeqN) ->
  orddict:fold(
    fun({N, Id}, Value, Acc) ->
      %io:format("Value ~w~n", [Value]),
      [{State, Ref2, Msg}] = Value,
      if Ref1 =:= Ref2 ->
        insert(Acc, {SeqN, Id}, {agreed, Ref2, Msg});
      true ->
        insert(Acc, {N, Id}, {State, Ref2, Msg})
      end
    end
    , new(), Queue).


%% popeo los mensj entregables
agreed(Queue) ->
  orddict:fold(fun agreed_aux/3, {[], new()}, Queue).

%% Si nuestro primer mensaje en la cola tiene un número de secuencia acordado puede ser entregado
agreed_aux(_, [{agreed, _, Msg}], {AccAgreed, []}) -> {[Msg | AccAgreed], []};

%% Si agregue algun mensajes a la cola, significa que hay anteriores a mi sin acordar
%% no hay mas entregables
agreed_aux(Seq, [Value], {AccAgreed, AccQueue}) -> {AccAgreed, insert(AccQueue, Seq, Value)}.
