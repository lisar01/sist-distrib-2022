-module(gui).

-export([start/2, init/2]).

-include_lib("wx/include/wx.hrl").

start(Name, Color) ->
    spawn(gui, init, [Name, Color]).

init(Name, Color) ->
    Width = 200,
    Height = 200,
    Server = wx:new(), %Server will be the parent for the Frame
    Frame = wxFrame:new(Server, -1, Name, [{size,{Width, Height}}]),
    wxFrame:setBackgroundColour(Frame,Color),
    wxFrame:show(Frame),
    loop(Frame).

loop(Frame)->
    receive
        {color, NewColor} ->
            wxFrame:setBackgroundColour(Frame,NewColor),
            wxFrame:refresh(Frame),
            loop(Frame);
        stop ->
            ok;
        Error ->
            io:format("gui: strange message ~w~n", [Error]),
            loop(Frame)
    end.