-module(retencion).

-export([new/0, add/2]).


new() -> [].

add(E1, []) -> [E1];
add(E1, [E2| Es]) ->
  Time1 = element(2,E1),
  Time2 = element(2,E2),
  case time:leq(Time1,Time2) of
    true ->
      [E1, E2| Es];
    false ->
      [E2 | add(E1, Es)]
  end.