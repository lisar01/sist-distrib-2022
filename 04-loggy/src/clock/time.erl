-module(time).

-export([zero/0, inc/2, merge/2, leq/2, clock/1, update/3, safe/2]).

zero() ->
    0.

inc(Name, T) ->
    T + 1.

merge(Ti, Tj) ->
    max(Ti, Tj).

leq(Ti, Tj) ->
    Ti =< Tj.

clock([]) -> [];
clock([N | Nodes]) -> [{N, 0}| clock(Nodes)]. 

update(_, _, []) -> [];
update(N, Time, [{N, _} | Clock]) -> [{N, Time} | Clock];
update(Node, Time, [{N, T} | Clock]) -> [{N, T} | update(Node, Time,Clock)].


safe(Time, Clock) ->
    lists:all(fun({_, T2}) -> leq(Time, T2) end, Clock).