-module(worker).

-export([start/5, stop/1, peers/2]).


start(Name, Logger, Seed, Sleep, Jitter) ->
    spawn_link(fun() -> init(Name, Logger, Seed, Sleep, Jitter) end).


stop(Worker) ->
    Worker ! stop.

init(Name, Log, Seed, Sleep, Jitter) ->
    random:seed(Seed, Seed, Seed),
    receive
        {peers, Peers} ->
            loop(Name, time:zero(), Log, Peers, Sleep, Jitter);
        stop ->
            ok
    end.

peers(Wrk, Peers) ->
    Wrk ! {peers, Peers}.

loop(Name, TimeL, Log, Peers, Sleep, Jitter) ->
    Wait = random:uniform(Sleep),
    receive
        {msg, Time, Msg} ->
            NewTime = time:inc(Name,time:merge(Time, TimeL)),
            Log ! {log, Name, NewTime, {received, Msg}},
            loop(Name, NewTime, Log, Peers, Sleep, Jitter);
        stop ->
            ok;
        Error ->
            Log ! {log, Name, TimeL, {error, Error}}
    after Wait ->
        Selected = select(Peers),
        NewTime = time:inc(Name, TimeL),
        Message = {hello, random:uniform(100)},
        Selected ! {msg, NewTime, Message},
        jitter(Jitter),
        Log ! {log, Name, NewTime, {sending, Message}},
        loop(Name, NewTime, Log, Peers, Sleep, Jitter)
    end.


select(Peers) ->
    lists:nth(random:uniform(length(Peers)), Peers).

jitter(0) -> ok;
jitter(Jitter) -> timer:sleep(random:uniform(Jitter)).