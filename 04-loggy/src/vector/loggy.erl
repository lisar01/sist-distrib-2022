-module(loggy).

-export([start/1, stop/1]).

start(Nodes) ->
    spawn_link(fun() -> init(Nodes) end).

stop(Logger) ->
    Logger ! stop.

init(Nodes) ->
    Retencion = retencion:new(),
    loop(time:clock(Nodes), Retencion).

loop(Clock, Retencion) ->
    receive 
        {log, From, Time, Msg} ->
            NewClock = time:update(From, Time, Clock),
            %io:format("CLOCK ~w~n", [NewClock]),
            NuevaRetencion = retencion:add({From, Time, Msg},  Retencion),
            %io:format("Lista de retencion ~w~n", [NuevaRetencion]),
            ListaDeRetencionSinImpresos = verificarRetencion(NuevaRetencion, NewClock),
            %io:format("Lista de retencion sin impresos ~w~n", [ListaDeRetencionSinImpresos]),
            loop(NewClock, ListaDeRetencionSinImpresos);
        stop ->
            ok
    end.


verificarRetencion([],_)-> [];
verificarRetencion([{From, Time, Msg}|T], Clock)  ->
    case time:safe(Time, Clock) of
        true ->
            log(From, Time, Msg),
            verificarRetencion(T, Clock);
        %% Como estan ordenados los eventos por su tiempo de menor a mayor
        %% si encuentro un evento no seguro de imprimir todos los siguientes
        %% a el lo seran, porque tienen un tiempo mayor, ocurrieron despues
        false ->  [{From, Time, Msg}|T]
    end.


log(From, Time, Msg) ->
    io:format("log: ~w ~w ~p~n", [Time, From, Msg]).



