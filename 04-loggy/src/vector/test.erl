-module(test).

-export([run/2]).


%report on your initial observations
run(Sleep, Jitter) ->
    Nodes = [john, paul, ringo, george],
    Log = loggy:start(Nodes),
    A = worker:start(john, Log, 13, Sleep, Jitter, Nodes),
    B = worker:start(paul, Log, 23, Sleep, Jitter, Nodes),
    C = worker:start(ringo, Log, 36, Sleep, Jitter, Nodes),
    D = worker:start(george, Log, 49, Sleep, Jitter, Nodes),
    worker:peers(A, [B, C, D]),
    worker:peers(B, [A, C, D]),
    worker:peers(C, [A, B, D]),
    worker:peers(D, [A, B, C]),
    timer:sleep(5000),
    loggy:stop(Log),
    worker:stop(A),
    worker:stop(B),
    worker:stop(C),
    worker:stop(D).