-module(time).

-export([zero/1, inc/2, merge/2, leq/2, clock/1, update/3, safe/2]).

zero(Peers) ->
    vector(Peers).

inc(_, []) -> [];
inc(N, [{N, Time} | Vector]) -> [{N, Time + 1 } | Vector];
inc(Node, [{N, T} | Vector]) -> [{N, T} | inc(Node, Vector)].

% para un vector chequeo el time de un worker y despues lo busco al worker en el otro vector
% y veo cual es el mas grande,

merge([], _)                   -> [];
merge([{Name, Time1}|T1], Tj) ->
    case Tj of
        []                  -> [];
        [{Name, Time2}|T2]  -> [{Name, max(Time1, Time2)}| merge(T1, T2)];
        [{N, Time2}|T2]     -> [{N, Time2}| merge(T1, T2)]
    end.



%%    {_, Time2} = get(Name,Tj),
%%    [{Name, max(Time1, Time2)} | merge(Time1, Ti)].

%%get(Name,Tj) -> lists:search(fun({N, _}) -> Name =:= N end, Tj).

leq(Ti, Tj) ->
    less_or_equals(Ti, Tj) and less(Ti, Tj).


less_or_equals([], _)               -> true;
less_or_equals([{_, Time1}|T1], Tj) ->
    case Tj of
        []              -> true;
        [{_, Time2}|T2] -> (Time1 =< Time2) and less_or_equals(T1, T2)
    end.

less([], _)               -> false;
less([{_, Time1}|T1], Tj) ->
    case Tj of
        []              -> false;
        [{_, Time2}|T2] -> (Time1 < Time2) or less(T1, T2)
    end.


vector([]) -> [];
vector([N | Nodes]) -> [{N, 0}| vector(Nodes)]. 

update(_, _, []) -> [];
update(N, Time, [{N, _} | Clock]) -> [{N, Time} | Clock];
update(Node, Time, [{N, T} | Clock]) -> [{N, T} | update(Node, Time,Clock)].


%%safe(Time, Clock) ->
%%    lists:all(fun({_, T2}) -> leq(Time, T2) end, Clock).

safe(Time, Clock) ->
    lists:all(fun({_, T2}) -> leq(Time, T2) end, Clock).

clock([]) -> [];
clock([N|Nodes]) -> [{N, vector([N|Nodes])} | clock(Nodes)].


% > < >  >
%[2,3,9,5]

%al menos uno menor si son iguales y ninguno mayor
%[1,3,10,5]