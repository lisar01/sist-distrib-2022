-module(worker).

-export([start/6, stop/1, peers/2]).


start(Name, Logger, Seed, Sleep, Jitter, Nodes) ->
    spawn_link(fun() -> init(Name, Logger, Seed, Sleep, Jitter, Nodes) end).


stop(Worker) ->
    Worker ! stop.

init(Name, Log, Seed, Sleep, Jitter, Nodes) ->
    random:seed(Seed, Seed, Seed),
    receive
        {peers, Peers} ->
            loop(Name, time:zero(Nodes), Log, Peers, Sleep, Jitter);
        stop ->
            ok
    end.

peers(Wrk, Peers) ->
    Wrk ! {peers, Peers}.


loop(Name, TimeL, Log, Peers, Sleep, Jitter) ->
    %io:format("TimeL: ~w~n", [TimeL]),
    Wait = random:uniform(Sleep),
    receive
        {msg, Time, Msg} ->
            %io:format("time:~w~n", [TimeL]),
            TimeMerge = time:merge(Time, TimeL),
            %io:format("time merge:~w~n", [TimeMerge]),
            NewTime = time:inc(Name,TimeMerge),
            %io:format("new time:~w~n", [NewTime]),
            Log ! {log, Name, NewTime, {received, Msg}},
            loop(Name, NewTime, Log, Peers, Sleep, Jitter);
        stop ->
            ok;
        Error ->
            Log ! {log, Name, TimeL, {error, Error}}
    after Wait ->
        Selected = select(Peers),
        NewTime = time:inc(Name, TimeL),
        Message = {hello, random:uniform(100)},
        Selected ! {msg, NewTime, Message},
        jitter(Jitter),
        Log ! {log, Name, NewTime, {sending, Message}},
        loop(Name, NewTime, Log, Peers, Sleep, Jitter)
    end.


select(Peers) ->
    lists:nth(random:uniform(length(Peers)), Peers).

jitter(0) -> ok;
jitter(Jitter) -> timer:sleep(random:uniform(Jitter)).