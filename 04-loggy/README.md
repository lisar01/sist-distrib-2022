## Loggy: un time logger lógico

### Implementacion de Loggy

### El test

-`Corramos algunos tests y tratemos de encontrar mensajes de log que sean
mostrados fuera de orden. ¿Cómo sabemos que fueron impresos fuera de
orden? Experimentemos con el jitter y veamos si podemos incrementar o
decrementar (¿eliminar?) el numero de entradas incorrectas.`

Con una prueba de 2 segundos de espera para que se envie el mensaje al par e informar al logger, se observa como en la linea 1 se imprime un "received" como primer mensaje, cosa que no deberia suceder sin un "sending" a anterior. Eso ya es una prueba de que los mensajes que imprime el logger estan fuera de orden.
![Consolas con_rudy secuencial](assets/Resultados-primer-test.png)


Al correr nuevamente el test con una prueba de 5 milisegundos de espera, se observa como se imprimen los "received" y los "sending" de manera intercalada, por lo que los mensajes que imprime el log están más ordenados comaparados al test anterior. Esto sucede ya que la espera, (de 5 milisegundos), es muy pequeña, por lo que los eventos de enviar un mensaje al par e informar al logger es casi casi inmediata y se reduce el tiempo para que ocurran eventos que desordenen los mensajes que imprime el logger.

![Consolas con_rudy secuencial](assets/ResultadosPrimerTestSinJitter.png)

### Tiempo Lamport

Hagamos algunos tests e identifiquemos situaciones donde las entradas de
log sean impresas en orden incorrecto. ¿Cómo identificamos mensajes que
estén en orden incorrecto? ¿Qué es siempre verdadero y qué es a veces
verdadero? ¿Cómo lo hacemos seguro?

### En el curso

También debemos escribir un reporte que describa el módulo
time (por favor no escribir una página de código fuente;
describirlo con sus propias palabras). Describir si encontraron entradas
fuera de orden en la primera implementación y en caso afirmativo, cómo
fueron detectadas. ¿Qué es lo que el log final nos muestra? ¿Los eventos
ocurrieron en el mismo orden en que son presentados en el log? ¿Que tan
larga será la cola de retención? Hacer algunos tests para tratar de
encontrar el máximo número de entradas.

### Vectores de relojes

`- ¿Qué diferencias habrían si se hubieran utilizado vectores de relojes? Es un
tema muy interesante. Si la primera parte de este ejercicio resultó fácil, intentemos implementar vectores de relojes. Descubriremos que no es tan complicado
y de hecho proveen algunos beneficios.`

La primera diferencia a la hora de utilizar vectores de relojes es su implementación.
<br>
Ahora los workers conocen a los nodos desde un principio con el objetivo de obtener un vector que registra sus timestamps.

La función start/6 del modulo worker quedaria de la siguiente forma:

```erlang
start(Name, Logger, Seed, Sleep, Jitter, Nodes) ->
    spawn_link(fun() -> init(Name, Logger, Seed, Sleep, Jitter, Nodes) end).
    

init(Name, Log, Seed, Sleep, Jitter, Nodes) ->
    random:seed(Seed, Seed, Seed),
    receive
        {peers, Peers} ->
            loop(Name, time:zero(Nodes), Log, Peers, Sleep, Jitter);
        stop ->
            ok
    end.
```
 La función zero/1 del modulo time queria de la siguiente forma:

```erlang
zero(Nodes) ->
    vector(Nodes).

vector([]) -> [];
vector([N | Nodes]) -> [{N, 0}| vector(Nodes)]. 
```
De esta forma el vector en cero de un worker seria algo de este estilo:

```erlang
[{Nodo1, 0},{Nodo2, 0},{Nodo3, 0} ...]
```
El modulo time sigue realizando la misma logica. La función inc/2 quedaría de la siguiente forma, donde se busca la marca de tiempo de un nodo en especifico y se incrementa en uno.

```erlang
inc(_, []) -> [];
inc(N, [{N, Time} | Vector]) -> [{N, Time + 1 } | Vector];
inc(Node, [{N, T} | Vector]) -> [{N, T} | inc(Node, Vector)].

```
La función merge/2 quedaría de la siguiente forma, donde se busca la marca de tiempo de un nodo en especifico y se queda con la mas alta, retornando un nuevo vector.

```erlang
merge([], _)                  -> [];
merge([{Name, Time1}|T1], Tj) ->
    case Tj of
        []                  -> [];
        [{Name, Time2}|T2]  -> [{Name, max(Time1, Time2)}| merge(T1, T2)];
        [{N, Time2}|T2]     -> [{N, Time2}| merge(T1, T2)]
    end.

```
La función leq/2 quedaría de la siguiente forma. Denota true cuando todos los nodos de un vector son menores o iguales a otro y cuando al menos uno de los nodos es menor a otro en un vector.

```erlang
leq(Ti, Tj) ->
    less_or_equals(Ti, Tj) and less(Ti, Tj).


less_or_equals([], _)               -> true;
less_or_equals([{_, Time1}|T1], Tj) ->
    case Tj of
        []              -> true;
        [{_, Time2}|T2] -> (Time1 =< Time2) and less_or_equals(T1, T2)
    end.

less([], _)               -> false;
less([{_, Time1}|T1], Tj) ->
    case Tj of
        []              -> false;
        [{_, Time2}|T2] -> (Time1 < Time2) or less(T1, T2)
    end.

```

La función update/3 quedaría de la siguiente forma. Donde busca el vector de un nodo en especifico y lo reemplaza, retornando un nuevo reloj actualizado.

```erlang
update(_, _, []) -> [];
update(N, Time, [{N, _} | Clock]) -> [{N, Time} | Clock];
update(Node, Time, [{N, T} | Clock]) -> [{N, T} | update(Node, Time,Clock)].
```

La función clock/1 quedaría de la siguiente forma.

```erlang
clock([]) -> [];
clock([N|Nodes]) -> [{N, vector([N|Nodes])} | clock(Nodes)].
```

De esta forma, un clock seria algo de este estilo:

```erlang
[
{Nodo1, [{Nodo1, 0},{Nodo2, 0},{Nodo3, 0} ...]},
{Nodo2, [{Nodo1, 0},{Nodo2, 0},{Nodo3, 0} ...]},
{Nodo3, [{Nodo1, 0},{Nodo2, 0},{Nodo3, 0} ...]},
... 
]
```

A la hora de correr el test uno de los resultados posibles es el siguiente:

![Consolas con_rudy secuencial](assets/Reloj%20de%20vectores%20test.png)

Uno de los beneficios que tiene utilizar relojes vectoriales es que, a diferencia de la implementación de los tiempos de Lamport, aca se puede ver de una manera mas clara como los nodos se comunican e interactuan entre ellos, ademas de otorgar un ordenamiento de los eventos.
Por ejemplo, de este test ejecutado se puede concluir la siguiente linea de eventos.

![Consolas con_rudy secuencial](assets/Tiempo%20logico.png)