-module(rudy).

-export([init/1, start/1, stop/0]).


start(Port) ->
    register(rudy, spawn(fun()-> init(Port)end)).

stop() ->
    exit(whereis(rudy), "time to die").

%% El procedimiento que inicializará el servidor, toma un número de puerto (por ejemplo 8080), 
%% abre un socket en modo escucha y pasa el socket a handler/1.
%% Una vez que el request fue procesado el socket se cerrará.
init(Port) ->
    Opt = [list, {active, false}, {reuseaddr, true}],
    case gen_tcp:listen(Port, Opt) of
        {ok, Listen} -> 
            handler(Listen),
            gen_tcp:close(Listen),
            ok;
        {error, _} ->
            error
        end.



%% Escuchará el socket esperando una conexión de entrada.
%% Una vez que un cliente se conecta pasara la conexión a request/1. Cuando el request haya sido
%% procesado se cerrará la conexión.
handler(Listen) ->
    case gen_tcp:accept(Listen) of
        {ok, Client} ->
%%          request(Client),
            spawn(fun()-> request(Client)end),
            handler(Listen);
        {error, _} ->
            error
    end.



%% Leerá el request desde la conexión del cliente y la parseará. 
%% Usara nuestro http parser y pasará la request a reply/1. La respuesta luego es enviada al cliente.

request(Client) ->
    Recv = gen_tcp:recv(Client, 0),
    case Recv of
        {ok, Str} ->
        Request = http:parse_request(Str),
        Response = reply(Request),
        gen_tcp:send(Client, Response);
    {error, Error} ->
        io:format("rudy: error: ~w~n", [Error])
    end,
    gen_tcp:close(Client).



%% Es donde decidimos qué responder, como convertir la respuesta en una respuesta HTTP bien formada.
reply({{get, URI, _}, _, _}) ->
    timer:sleep(40),
    http:ok(http:get(URI)).
