-module(rudy2).

-export([init/1, start/1, stop/0, handler/1]).


start(Port) ->
  register(rudy2, spawn(fun()-> init(Port)end)).

stop() ->
  rudy2 ! stop.

%% El procedimiento que inicializará el servidor, toma un número de puerto (por ejemplo 8080),
%% abre un socket en modo escucha y pasa el socket a handler/1.
%% Una vez que el request fue procesado el socket se cerrará.
init(Port) ->
  Opt = [list, {active, false}, {reuseaddr, true}],
  case gen_tcp:listen(Port, Opt) of
    {ok, Listen} ->
      [spawn(rudy2, handler,[Listen]) || _ <- lists:seq(1,25)],
      receive
        stop ->
          gen_tcp:close(Listen),
          ok
      end;
    {error, _} ->
      error
  end.



%% Escuchará el socket esperando una conexión de entrada.
%% Una vez que un cliente se conecta pasara la conexión a request/1. Cuando el request haya sido
%% procesado se cerrará la conexión.
handler(Listen) ->
  case gen_tcp:accept(Listen) of
    {ok, Client} ->
      io:format("Client: ~w~n", [Client]),
      request(Client),
      timer:sleep(100),
      handler(Listen);
    {error, _} ->
      error
  end.



%% Leerá el request desde la conexión del cliente y la parseará.
%% Usara nuestro http parser y pasará la request a reply/1. La respuesta luego es enviada al cliente.

request(Client) ->
  Recv = gen_tcp:recv(Client, 0),
  case Recv of
    {ok, Str} ->
      Request = http:parse_request(Str),
      Response = reply(Request),
      gen_tcp:send(Client, Response);
    {error, Error} ->
      io:format("rudy: error: ~w ~w ~n", [Error, Client])
  end,
  io:format("Closing Client: ~w~n", [Client]).
%%  gen_tcp:shutdown(Client,write).



%% Es donde decidimos qué responder, como convertir la respuesta en una respuesta HTTP bien formada.
reply({{get, URI, _}, _, _}) ->
  timer:sleep(40),
  http:ok(http:get(URI)).
