# Rudy: un pequeño web server

## El ejercicio

### Se debe completar el server rudimentario descripto arriba y hacer algunos experimentos.
<br>
 En primer lugar, la función init/1 inicializa el servidor, toma un numero de puerto, abre un socket en modo escucha y pasa el socket a handler/1.

``` erlang
init(Port) ->
    Opt = [list, {active, false}, {reuseaddr, true}],
    case gen_tcp:listen(Port, Opt) of
        {ok, Listen} ->
            :
            gen_tcp:close(Listen),
            ok;
    {error, Error} ->
        error
    end.
```

Por lo que la línea que queda por completar es la que llama a handler/1, pasandole el socket:

``` erlang
handler(Listen)
```
La función init/1 quedaría de la siguiente forma:

```erlang
init(Port) ->
    Opt = [list, {active, false}, {reuseaddr, true}],
    case gen_tcp:listen(Port, Opt) of
        {ok, Listen} -> 
            handler(Listen),
            gen_tcp:close(Listen),
            ok;
        {error, _} ->
            error
        end.
```

En segundo lugar, la función handler/1 escuchará el socket esperando una conexión de entrada y una vez que el cliente se conecta le pasará la conexión a request/1.

``` erlang
handler(Listen) ->
    case gen_tcp:accept(Listen) of
        {ok, Client} ->
            :
        {error, Error} ->
            error
    end.
```
Por lo que las líneas que quedan por completar son las que llaman a request/1 y la llamada recursiva para que se escuche una nueva conexión una vez que la primera haya sido procesada. 

``` erlang
request(Client),
handler(Listen);
```

La función handler/1 quedaría de la siguiente forma:

``` erlang
handler(Listen) ->
    case gen_tcp:accept(Listen) of
        {ok, Client} ->
            request(Client),
            handler(Listen);
        {error, _} ->
            error
    end.
```
En tercer lugar, la función request/1 leerá el request desde la conexión del cliente y, utilizando la función parse_request/1 del módulo http, la parseará. Luego, le pasará la request a reply/1 y se le pasará la respuesta al cliente.

``` erlang
request(Client) ->
    Recv = gen_tcp:recv(Client, 0),
        case Recv of
        {ok, Str} ->
            :
            Response = reply(Request),
            gen_tcp:send(Client, Response);
        {error, Error} ->
            io:format("rudy: error: ~w~n", [Error])
        end,
        gen_tcp:close(Client).
```
Por lo que la línea que queda por completar es la que declara la variable Request y utiliza la función parse_request/1 del modulo http.

``` erlang
Request = http:parse_request(Str),
```

La función request/1 quedaría de la siguiente forma:

``` erlang
request(Client) ->
    Recv = gen_tcp:recv(Client, 0),
        case Recv of
        {ok, Str} ->
            Request = http:parse_request(Str), 
            Response = reply(Request),
            gen_tcp:send(Client, Response);
        {error, Error} ->
            io:format("rudy: error: ~w~n", [Error])
        end,
        gen_tcp:close(Client).
```

#
Otras modificaciones que se realizaron al server son la creación de procesos para que no quede ocioso mientras otro request esta listo para ser parseado. Mas precisamente en la función handler/1.

```erlang
handler(Listen) ->
    case gen_tcp:accept(Listen) of
        {ok, Client} ->
            spawn(fun()-> request(Client)end),
            handler(Listen);
        {error, _} ->
            error
    end.
```
Se crea un proceso nuevo a la hora de pasarle la conexión a la función request/1, en donde se parseara la request. De esta forma no es necesario esperar a que se parsee la request para escuchar una nueva conexión.

Otra modificación que realizo en el proyecto es la creación de procesos en la función run/3 del módulo test. De esta forma la llamada recursiva no queda esperando el procesamiento de la función request/2, que una vez conectado genera un request.

``` erlang
run(N, Host, Port) ->
  if
    N == 0 ->
      ok;
    true ->
      spawn(fun()-> request(Host,Port)end),
      run(N-1, Host, Port)
  end.
```


### ¿Cuántos requests por segundo podemos servir?
``` erlang
    1> test:bench('localHost', 8080).
    4242368
```
Con el delay artificial el server tardó en responder 4,24 s. Entonces podemos recibir aprox. 23 requests por s.

### ¿Nuestro delayar artificial es importante o desaparece dentro del overhead de parsing?**
Sin el delay las respuestas son muchos más rápidas, se pueden llegar a servir 100 requests en 0,07 s.
Eso significa que como el delay es de 40 ms y con este el server tarda aprox. 4 s, lo que más importa es
el delay, no el overhead.

**Con delay:**

``` erlang
reply({{get, URI, _}, _, _}) ->
    timer:sleep(40),
    http:ok(http:get(URI)).
```

``` erlang
7> test:bench('localHost', 8080).
4237819
```

**Sin delay:**

``` erlang
reply({{get, URI, _}, _, _}) ->
%%  timer:sleep(40),
    http:ok(http:get(URI)).
```

``` erlang
7> test:bench('localHost', 8080).
77853
```

### ¿Qué ocurre si ejecutamos los benchmarks en varias máquinas al mismo tiempo? Hacer algunos tests y reportemos los resultados.

Cuando corremos el servidor y está  implementado de forma secuencial, tarda muchisimo en responder, 14 segundos para contestar 100 request.
Esto pasa porque espera que termine la request antes de hacer la otra, y ahora hay muchas más.

**Server rudy sin concurrencia:**

``` erlang

handler(Listen) ->
    case gen_tcp:accept(Listen) of
        {ok, Client} ->
            request(Client),
            handler(Listen);
        {error, _} ->
            error
    end.

```
![Consolas con_rudy secuencial](assets/Bench_varias_maquinas_rudy_no_concurrente.png)

Pero cuando lo corremos y trabaja de forma concurrente, casi no hay diferencia como
cuando espera a una sola máquina. Solo afecta el delay artificial.

**Server Rudy con concurrencia:**

``` erlang
handler(Listen) ->
    case gen_tcp:accept(Listen) of
        {ok, Client} ->
            spawn(fun()-> request(Client)end),
            handler(Listen);
        {error, _} ->
            error
    end.
```
![Consolas con rudy_concurrente](assets/Bench_varias_maquinas.png)
