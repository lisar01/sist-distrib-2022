-module(test2).
-export([bench/2, check_finished/3, request/3]).

bench(Host, Port) ->
  Start = erlang:system_time(micro_seconds),
  P = spawn(test2, check_finished, [self(),0, 0]),
  [spawn(test2, request,[Host, Port, P]) || _ <- lists:seq(1,100)],
  receive
    Finish ->
      io:format("~w~n", [Finish - Start])
  end.

check_finished(Pid, Max_Finish, 100) -> Pid ! Max_Finish;

check_finished(Pid, Max_Finish, Requests_Made) ->
  receive
    Finished_Time ->
      check_finished(Pid, max(Finished_Time, Max_Finish), Requests_Made + 1)
  end.

request(Host, Port, Pid) ->
  Opt = [list, {active, false}, {reuseaddr, true}],
  {ok, Server} = gen_tcp:connect(Host, Port, Opt),
  gen_tcp:send(Server, http:get("foo")),
  Recv = gen_tcp:recv(Server, 0),
  case Recv of
    {ok, _} ->
      ok;
    {error, Error} ->
      io:format("Server: ~w~n", [Server]),
      io:format("test: error: ~w~n", [Error])
  end,
  Finish = erlang:system_time(micro_seconds),
  Pid ! Finish,
  gen_tcp:close(Server).
