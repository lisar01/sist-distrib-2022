## Muty: Lock de exclusión mutua distribuido


### Lock1
El `lock1` tiene una implementación simple para evitar que no haya múltiples workers en la zona crítica y lo único que cambiamos
del código fue mostrar la pantalla color verde cuando hay un deadlock en el módulo `gui`.

Como ya venía implementado, lo único que teníamos que hacer era testearlo a través del módulo `muty` con diferentes valores Sleep y Work.
Sleep es el tiempo que el worker duerme antes de entrar a la zona crítica y Work, el tiempo que duerme mientras está en la zona crítica
con el lock. En conclusión:
- **Sleep bajo**, el worker está poco tiempo en la zona no crítica, el lock está más demandado
- **Sleep alto**, el worker está más tiempo en la zona no crítica, el lock está menos demandado
- **Work bajo**, el worker está menos tiempo en la zona crítica, el lock pasa de worker a worker más rápido
- **Work alto**, el worker está más tiempo en la zona crítica, los otros workers tienen más chances de hacer deadlock

Para obtener el lock, es una carrera entre los workers. Las conclusiones se verifican al correrlo:

```erlang
muty:start(lock1, 4000, 4000).
John: 22 locks taken, average of 2387.6281363636363 ms, 6 deadlock situations
Paul: 17 locks taken, average of 2054.2224705882354 ms, 9 deadlock situations
George: 19 locks taken, average of 2516.3775263157895 ms, 7 deadlock situations
Ringo: 23 locks taken, average of 2060.1766956521737 ms, 4 deadlock situations

muty:start(lock1, 10, 4000).
10>Paul: 8 locks taken, average of 2582.246375 ms, 9 deadlock situations
12> John: 8 locks taken, average of 2257.86875 ms, 9 deadlock situations
12> George: 12 locks taken, average of 2584.3881666666666 ms, 4 deadlock situations
12> Ringo: 11 locks taken, average of 2955.813181818182 ms, 5 deadlock situations

muty:start(lock1, 4000, 10).
John: 58 locks taken, average of 0.15020689655172412 ms, 1 deadlock situations
Ringo: 48 locks taken, average of 0.2025625 ms, 1 deadlock situations
Paul: 48 locks taken, average of 77.32689583333332 ms, 0 deadlock situations
George: 59 locks taken, average of 47.617677966101695 ms, 0 deadlock situations

muty:start(lock1, 10, 10).
10> Paul: 33 locks taken, average of 1229.4639393939394 ms, 26 deadlock situations
10> George: 30 locks taken, average of 554.8714333333334 ms, 32 deadlock situations
10> Ringo: 42 locks taken, average of 1439.7659523809525 ms, 21 deadlock situations
10> John: 39 locks taken, average of 1347.510358974359 ms, 23 deadlock situations
```

#### En la función `held`¿Por qué no estamos esperando mensajes ok?
El lock espera mensajes `ok` para saber que otros locks se liberaron y poder obtener el recurso, si el lock ya tiene
el recurso no es necesario esperarlos.

#### ¿Funciona bien? ¿Qué ocurre cuando incrementamos el riesgo de un conflicto de lock? ¿Por qué?
Funciona bien. Al aumentar el riesgo de conflicto de lock, ocurrirán más deadlocks. Esto puede pasar cuando el Sleep
es muy bajo y el Work muy alto o los dos son muy bajos. Es decir, cuando muchos workers demandan ir a la zona crítica o
un worker está mucho tiempo en ella



### Lock2
`lock2` es una implementación por prioridad. Para hacer esto no fueron necesarios muchos cambios. Se creó un nuevo módulo
`lock2` tomando como template el `lock1`, los otros archivos no fueron modificados.

Un lock tiene un número que le da prioridad frente a otros locks. Para hacer esto, se agregó un parámetro más, Id, a las
funciones `open`, `requests`, `wait` y `held`. Ademas, como decia en la consigna *Un lock en el estado de espera enviará un mensaje
ok a un lock que lo solicite si el lock tiene una prioridad más alta*, si alguien solicita el lock no lo agrego mas a la cola
de espera para avisarle, sino que hago esto:

```erlang
{request, From, FromId, Ref} ->
  if FromId < Id ->
    From ! {ok, Ref},
    wait(Id, Nodes, Master, [Ref|Refs], Waiting);
  true ->
    wait(Id, Nodes, Master, Refs, [{From, Ref}|Waiting])
  end;
```
Si el lock tiene más prioridad, le envió un mensaje `ok` y agregó su Ref a mi lista de Refs. Esto último lo hacemos, porque
si no, múltiples workers entraran a la zona crítica a la vez. Con esto nos aseguramos, que solo esta libre cuando el lock
con mayor prioridad termine. Si tengo mas prioridad, lo agrego a mi lista de espera para avisar una vez que termine.

#### ¿Qué tan eficientemente lo hace y cuál es la desventaja?
Es eficiente, pero la desventaja es que mientras menos prioridad tenga un lock, menos chances de tener el recurso y más
de entrar en deadlock.

```erlang
17> Ringo: 11 locks taken, average of 1005.2889090909091 ms, 34 deadlock situations
17> John: 82 locks taken, average of 490.7957195121951 ms, 0 deadlock situations
17> Paul: 0 locks taken, average of 0.0 ms, 41 deadlock situations
17> George: 2 locks taken, average of 903.2705 ms, 41 deadlock situations
```


### Lock3

En el modulo lock3 la estrategia implementada es utilizando relojes logicos de Lamport. Esto nos garantiza que que los locks son tomados con una prioridad dada por el orden temporal.
La primer gran diferencia con los 2 modulos anteriores es la utilización de una variable `clock` que funciona como marca de tiempo. <BR>
El clock es inicializado en 0 en una primera instancia, luego con cada mensaje de `request` u `ok` que le llega desde otro lock con su respectiva marca de tiempo, el `clock` es actualizado e incrementado. Esto ocurre en los 3 estados: open, wait y held.
Estas operaciones que manipulan las marcas de tiempo estan abstraidas en el modulo `time` (similar a proyectos anteriores), de esta forma el modulo lock no sabe como estan desarrollados esas funciones y se desentiende de su implementacion, solo utiliza una interfaz que le es proveida. 

open/3
```erlang
open(Id, Nodes, MyClock) ->
  receive
    {take, Master} ->
      Refs = requests(Id, Nodes, MyClock),
      wait(Id, Nodes, Master, Refs, [], MyClock, MyClock);
    {request, From, Ref, FromClock, _} ->
        NewClock = time:inc(time:merge(MyClock, FromClock)),
        From ! {ok, Ref, NewClock},
        open(Id, Nodes, NewClock);
    stop ->
      ok
  end.
```

wait/7

En esta funcion se puede observar las 3 marcas de tiempo diferentes que se utilizan. `MyClock` como la marca de tiempo del lock que esta utilizando el lock actual, `FromClock` que es la marca de timepo que llega en un mensaje desde otro lock, y finalmente, `CurrentClock` que es la marca de tiempo mas alta del vista hasta el momento. <br>
A diferencia del modulo lock2, la prioridad en la condición del `if` es dada por las marcas de tiempo. Un request tiene mas prioridad para tomar el lock si su marca de tiempo es menor o igual a la marca de tiempo actual. Si esto no se determina, como "desempate" se utilizan los identificadores de cada lock. Tendrá mas prioridad el lock con menor `Id`.

```erlang
wait(Id, Nodes, Master, Refs, Waiting, MyClock, CurrentClock) ->
  receive
    {request, From, Ref, FromClock, FromId} ->
      IsSafe = time:leq(FromClock, CurrentClock),
      IsEquals = time:equals(FromClock, CurrentClock),
      NewClock = time:inc(time:merge(FromClock, MyClock)),
      if
        IsSafe or (FromId > Id and IsEquals)->
          From ! {ok, Ref, time:inc(NewClock)},
          wait(Id, Nodes, Master, [Ref|Refs], Waiting, NewClock, CurrentClock);
        true ->
          wait(Id, Nodes, Master, Refs, [{From, Ref}|Waiting], NewClock, CurrentClock)
      end;
    {ok, Ref, FromClock} ->
      NewClock = time:inc(time:merge(FromClock, MyClock)),  
      Refs2 = lists:delete(Ref, Refs),
      wait(Id, Nodes, Master, Refs2, Waiting, NewClock, CurrentClock);
    release ->
      ok(Waiting, MyClock),
      open(Id, Nodes, MyClock)
  end.
```
held/4

En esta funcion tambien se observa como cuando le llega una marca de tiempo de otro lock, el clock es actualizado (se queda con la marca de tiempo mas alta) y se incrementa en una unidad.

```erlang
held(Id, Nodes, Waiting, MyClock) ->
  receive
    {request, From, Ref, FromClock, _} ->
      NewClock = time:inc(time:merge(MyClock, FromClock)),
      held(Id, Nodes, [{From, Ref}|Waiting], NewClock);
    release ->
      ok(Waiting, MyClock),
      open(Id, Nodes, MyClock)
  end.
```


¿Puede darse la situación en que un worker no se le da prioridad al lock a pesar
de que envió el request a su lock con un tiempo lógico anterior al worker que lo
consiguió?

No puede darse esa situación ya que la estrategia de los relojes de lamport no lo permite. Los relojes se actualizan incrementalmente, y la condicion a la hora de dar prioridad tiene en cuenta la marca de tiempo mas baja. Puede darse la situacion en que un worker no se le de la prioridad al lock a pesar de que envio el request a su lock con un tiempo logico igual al worker que lo consiguio, en este escenario, el lock lo consigue el nodo del lock con un identificador de menor valor, es decir, con mas prioridad.

Los resultados a la hora de correr el test muty con el modulo lock3 son mas deseables comparado a las estrategias anteriores. <br>
Se puede observar como desciende la taza de deadlocks y los workers acceden al lock de una manera mas equitativa.
Esta prueba esta realizada con un valor de sleep igual a 1000 milisegundos y un valor de work igual a 2000 milisegundos, este escenario presenta una mayor probabilidad de que exista congestion.

```erlang
Paul: lock taken in 1029 ms
George: lock taken in 1085 ms
Ringo: lock taken in 831 ms
John: lock taken in 1562 ms
Paul: lock taken in 1726 ms
George: lock taken in 2446 ms
Ringo: lock taken in 1000 ms
John: lock taken in 956 ms       
George: lock taken in 1046 ms    
6> muty:stop().
John: 16 locks taken, average of 854.6636875 ms, 0 deadlock situations
stop
George: 16 locks taken, average of 895.844375 ms, 0 deadlock situations
Paul: lock taken in 794 ms
Paul: 14 locks taken, average of 1081.8477857142857 ms, 0 deadlock situations
Ringo: lock taken in 705 ms
Ringo: 16 locks taken, average of 856.2374375 ms, 0 deadlock situations

```

En este test no hubo casos de deadlocks, y equitativamente accedieron al lock, Paul con 14 veces, John con 16 veces, Ringo con 16 veces y George con 16 veces. <br>
Se pueden concluir las mismas observaciones si es que se modifican los valores de sleep y work.

Work=3000, Sleep=3000.
```erlang
George: 5 locks taken, average of 1065.7233999999999 ms, 2 deadlock situations
Ringo: 6 locks taken, average of 1784.8695 ms, 1 deadlock situations
John: lock taken in 3344 ms
John: 6 locks taken, average of 2324.3985 ms, 0 deadlock situations
Paul: lock taken in 3372 ms
Paul: 6 locks taken, average of 2558.669 ms, 0 deadlock situations
```
Siguen accediento al lock de manera equitativa (5, 6, 6, 6 veces respectivamente) y los casos de deadlocks se ven disminuidos comparados a los modulos de lock anteriores.
Por estos motivos, esta estregia de utilizar relojes logicos como solucion prensenta ventajas por sobre las soluciones anteriores. 