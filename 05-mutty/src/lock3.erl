-module(lock3).

-export([start/1]).

start(Id) ->
  spawn(fun() -> init(Id) end).

init(Id) ->
  receive
    {peers, Peers} ->
      open(Id, Peers, time:zero());
    stop ->
      ok
  end.

open(Id, Nodes, MyClock) ->
  receive
    {take, Master} ->
      Refs = requests(Id, Nodes, MyClock),
      wait(Id, Nodes, Master, Refs, [], MyClock, MyClock);
    {request, From, Ref, FromClock, _} ->
        NewClock = time:inc(time:merge(MyClock, FromClock)),
        From ! {ok, Ref, NewClock},
        open(Id, Nodes, NewClock);
    stop ->
      ok
  end.

requests(Id, Nodes, MyClock) ->
  UpdateNodes = lists:delete(self(), Nodes),
  lists:map(fun(P) -> R = make_ref(), P ! {request, self(), R, MyClock, Id}, R end, UpdateNodes).


wait(Id, Nodes, Master, [], Waiting, MyClock, _) ->
  Master ! taken,
  held(Id, Nodes, Waiting, MyClock);
  
wait(Id, Nodes, Master, Refs, Waiting, MyClock, CurrentClock) ->
  receive
    {request, From, Ref, FromClock, FromId} ->
      IsSafe = time:leq(FromClock, CurrentClock),
      IsEquals = time:equals(FromClock, CurrentClock),
      NewClock = time:inc(time:merge(FromClock, MyClock)),
      if
        IsSafe or (FromId > Id and IsEquals)->
          From ! {ok, Ref, time:inc(NewClock)},
          wait(Id, Nodes, Master, [Ref|Refs], Waiting, NewClock, CurrentClock);
        true ->
          wait(Id, Nodes, Master, Refs, [{From, Ref}|Waiting], NewClock, CurrentClock)
      end;
    {ok, Ref, FromClock} ->
      NewClock = time:inc(time:merge(FromClock, MyClock)),  
      Refs2 = lists:delete(Ref, Refs),
      wait(Id, Nodes, Master, Refs2, Waiting, NewClock, CurrentClock);
    release ->
      ok(Waiting, MyClock),
      open(Id, Nodes, MyClock)
  end.

ok(Waiting, MyClock) ->
  lists:foreach(fun({F,R}) -> F ! {ok, R, MyClock} end, Waiting).


held(Id, Nodes, Waiting, MyClock) ->
  receive
    {request, From, Ref, FromClock, _} ->
      NewClock = time:inc(time:merge(MyClock, FromClock)),
      held(Id, Nodes, [{From, Ref}|Waiting], NewClock);
    release ->
      ok(Waiting, MyClock),
      open(Id, Nodes, MyClock)
  end.