## Paxy: el protocolo Paxos

##### Versión 1: Basica


### Mejoras

##### Versión 2: Acceptor con algun delay e ignorando mensajes de sorry

##### Versión 3: Tolerante a Fallos

##### Versión 4: Elección para decidir el Proposer activo

##### Versión 5: Abortar si hay mayoria de envios de sorry

### Presentación
[Link ppt de la entrega final](https://docs.google.com/presentation/d/1j6COLHEoeP5hzOSc_FEmiJc13yGZhhUQgiDcwUS_Utk/edit?usp=drivesdk)
