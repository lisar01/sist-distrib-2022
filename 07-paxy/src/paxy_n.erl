-module(paxy_n).

-export([start/5, stops/1, crash/1]).

-define(sleep, 1000).

%% Modulo paxy mas configurable: start toma como parametros el modulo de acceptor y proposal
%% a usar y la lista de Acceptors (lista de atomos nombres) y Proposers (lista de tuplas
%% de atomo nombre y valor a proponer)

start(Seed, AcceptorMod, ProposalMod, Acceptors, Proposals) ->
  lists:foreach(fun(A) -> register(A, apply(AcceptorMod, start, [A])) end, Acceptors),
  lists:foldr(fun({Name, Value}, Acc) ->
    apply(ProposalMod, start, [Name, Value, Acceptors, Seed+Acc]),
    Acc + 1
    end,
    1, Proposals),
  Proposals.

stops(Proposals) ->
  lists:foreach(fun(P) -> stop(P) end, Proposals).

stop(Name) ->
  case whereis(Name) of
    undefined ->
      ok;
    Pid ->
      Pid ! stop
  end.

%% Usado para simular un crash en un Acceptor para la version 3, tolerante a fallos

crash(Name) ->
  case whereis(Name) of
    undefined ->
      ok;
    Pid ->
      unregister(Name),
      exit(Pid, "crash"),
      io:format("Crash proceso ~w~n", [Name]),
      timer:sleep(?sleep),
      io:format("Vuelve proceso ~w~n", [Name]),
      register(Name, v3acceptor:start(Name))
  end.