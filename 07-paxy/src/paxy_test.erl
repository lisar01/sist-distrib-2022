-module(paxy_test).

-export([start1/3, stop1/0, start2/3, start_and_crash/2, stop2/0, start3/3, stop3/0, start4/2]).

-define(wait, 1000).

-define(Acceptors1, [carl,lenny,moe,skinner]).
-define(Acceptors2, [a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z]).
-define(Acceptors3, [carl,lenny,moe,skinner,smithers,selma]).

-define(Proposers1, [{homer, donuts}, {barney, tabern}, {flanders, church}]).
-define(Proposers2, [{homer, donuts}, {barney, tabern}, {flanders, church}, {marge, shop}, {milhouse, school}]).
-define(Proposers3, [{kurtz1, green}, {will1, red}, {kilgore1, blue},
  {kurtz2, yellow}, {will2, white}, {kilgore2, brown},
  {john, silver}, {paul, gold}, {ringo, orange},
  {bimbo, pink}, {luxemburgo, purple}, {dinamark, violet},
  {kurtz3, light}, {will3, grey}, {saopaolo, black}
]).

%% caso dado
start1(Seed, AcceptorMod, ProposerMod) ->
  paxy_n:start(Seed, AcceptorMod, ProposerMod, ?Acceptors1, ?Proposers1).

stop1() ->
  paxy_n:stops(?Acceptors1).

%% mas aceptors que proposers
start2(Seed, AcceptorMod, ProposerMod) ->
  paxy_n:start(Seed, AcceptorMod, ProposerMod, ?Acceptors2, ?Proposers2).

stop2() ->
  paxy_n:stops(?Acceptors2).

%% mas proposers que aceptors
start3(Seed, AcceptorMod, ProposerMod) ->
  paxy_n:start(Seed, AcceptorMod, ProposerMod, ?Acceptors3, ?Proposers3).

stop3() ->
  paxy_n:stops(?Acceptors3).

%% tolerante a fallos, arranco y crasheo acceptors
start_and_crash(Seed, ToCrash) ->
  paxy_n:start(Seed, v3acceptor, v3proposer, ?Acceptors3, ?Proposers1),
  timer:sleep(?wait),
  lists:foreach(fun(A) -> paxy_n:crash(A) end, ToCrash).


%% con eleccion de proposer activo
start4(Seed, AcceptorMod) ->
  v4paxy_n:start(Seed, AcceptorMod, ?Acceptors3, ?Proposers2).
