-module(v3proposer).

-export([start/4]).

-define(timeout, 200).
-define(backoff, 10).
-define(delay, 20).


%% Version tolerante a fallos

start(Name, Proposal, Acceptors, Seed) ->
  spawn(fun() -> init(Name, Proposal, Acceptors, Seed) end).

init(Name, Proposal, Acceptors, Seed) ->
  random:seed(Seed, Seed, Seed),
  Round = order:one(Name),
  round(Name, ?backoff, Round, Proposal, Acceptors).

round(Name, Backoff, Round, Proposal, Acceptors) ->
  case ballot(Round, Proposal, Acceptors) of
    {ok, Decision} ->
      io:format("----------------------------------------------~w decided ~w in round ~w~n", [Name, Decision, Round]),
      {ok, Decision}; %%Verificar si es la tupla
    abort ->
      timer:sleep(random:uniform(Backoff)),
      Next = order:inc(Round),
      round(Name, (2 * Backoff), Next, Proposal, Acceptors)
  end.

ballot(Round, Proposal, Acceptors) ->
  prepare(Round, Acceptors),
  Quorum = (length(Acceptors) div 2) + 1,
  Max = order:null(),
  case collect(Quorum, Round, Max, Proposal) of
    {accepted, Value} ->
      accept(Round, Value, Acceptors), %%Proposal?
      case vote(Quorum, Round) of
        ok ->
          {ok, Value};
        abort ->
          abort
      end;
    abort ->
      abort
  end.

collect(0, _, _, Proposal) -> {accepted, Proposal}; %%Cuando incrementar el valor MAX?
collect(N, Round, Max, Proposal) ->
  receive
    {promise, Round, _, na} ->
      collect((N-1), Round, Max, Proposal);
    {promise, Round, Voted, Value} ->
      %io:format("Promise a round: ~w voted:~w value:~w~n", [Round, Voted, Value]),
      case order:gr(Voted, Max) of %%Round?
        true ->
          io:format("Valor ~w reemplaza a ~w ~n", [Value, Proposal]),
          collect((N-1), Round, Voted, Value);
        false ->
          collect((N-1), Round, Max, Proposal)
      end;
    {promise, _, _, _} ->
      collect(N, Round, Max, Proposal);
    {sorry, Round} ->
      collect(N, Round, Max, Proposal);
    {sorry, _} ->
      collect(N, Round, Max, Proposal)
  after ?timeout ->
    abort
  end.

vote(0, _) -> ok;
vote(N, Round) ->
  receive
    {vote, Round} ->
      vote((N-1), Round);
    {vote, _} ->
      vote(N, Round);
    {sorry, Round} ->
      vote(N, Round);
    {sorry, _} ->
      vote(N, Round)
  after ?timeout ->
    abort
  end.

prepare(Round, Acceptors) ->
  Fun = fun(Acceptor) -> sendAcceptor(Acceptor, {prepare, self(), Round}) end,
  lists:map(Fun, Acceptors).

accept(Round, Proposal, Acceptors) ->
  Fun = fun(Acceptor) -> sendAcceptor(Acceptor, {accept, self(), Round, Proposal}) end,
  lists:map(Fun, Acceptors).


%% Chequea si el Acceptor esta vivo o no antes de enviar el mensaje
sendAcceptor(Acceptor, Message) ->
  case whereis(Acceptor) of
    undefined ->
      down;
    Pid ->
      Pid ! Message,
      timer:sleep(random:uniform(?delay))
  end.

