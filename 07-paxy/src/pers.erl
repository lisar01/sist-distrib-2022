-module(pers).

-export([read/1, store/4, delete/1]).


read(Id) ->
  {ok, Id} = dets:open_file(Id, []),
  case dets:lookup(Id, perm) of
    [{perm, Promise, Voted, Accepted}] ->
      {Promise, Voted, Accepted};
    [] ->
      {order:null(), order:null(), na}
  end.

store(Id, Promise, Voted, Accepted) ->
  dets:insert(Id, {perm, Promise, Voted, Accepted}).

delete(Id) ->
  dets:delete(Id, perm),
  dets:close(Id).
