-module(v3acceptor).

-export([start/1]).

-define(sleep, 100).

%% Version tolerante a fallos

start(Name) ->
  spawn(fun() -> init(Name) end).

%% persisto el estado de un acceptor, si es que tuvo un estado anterior tomo ese
%% sino se crea con valores default
init(Name) ->
  {Promise, Voted, Accepted} = pers:read(Name),
  io:format("Init Acceptor ~w con estado {Promise ~w, Voted ~w, Accepted ~w} ~n", [Name, Promise, Voted, Accepted]),
  pers:store(Name, Promise, Voted, Accepted),
  acceptor(Name, Promise, Voted, Accepted).


acceptor(Name, Promise, Voted, Accepted) ->
  %% duerme para simular mejor el algoritmo, y no termine antes que vuelva un nodo crasheado
  timer:sleep(?sleep),
  receive
    {prepare, Proposer, Round} ->
      case order:gr(Round,Promise) of
        true ->
          Proposer ! {promise, Round, Voted, Accepted},
          pers:store(Name, Round, Voted, Accepted),
          acceptor(Name, Round, Voted, Accepted); %%Se actualiza la ultima promesa dada
        false ->
          Proposer ! {sorry, Round}, %%{sorry, Promise}? informarle que prometimos no votar en la ronda solicitada por el proponente
          acceptor(Name, Promise, Voted, Accepted) %%Prometimos no votar en rondas menores a mi promise
      end;
    {accept, Proposer, Round, Proposal} ->
      io:format("me propuso ~w con round~w:~n", [Proposal, Round]),
      case order:goe(Round, Promise) of %%{Promise, Round}
        true ->
          Proposer ! {vote, Round}, %%aceptar la solicitud
          case order:goe(Round, Voted) of %%Si aceptamos un valor en una ronda inferior
            true ->
              io:format("~w ultimo valor aceptado: ~w~n", [Name, Proposal]),
              pers:store(Name, Round, Voted, Accepted),
              acceptor(Name, Promise, Round, Proposal); %%aún se debe recordar el valor del número de votación más alto%
            false ->
              acceptor(Name, Promise, Voted, Accepted) %%Accepted = ultimo valor aceptado
          end;
        false ->
          Proposer ! {sorry, Round},
          io:format("~w no pudo aceptar ~w~n", [Name, Proposal]),
          acceptor(Name, Promise, Voted, Accepted)
      end;
    stop ->
      pers:delete(Name),
      unregister(Name),
      ok
  end.
