-module(v2acceptor).

-export([start/1]).

-define(sleep, 10).

%%Version Acceptor con algun delay e ignorando mensajes de sorry.

start(Key) ->
  spawn(fun() -> init(Key) end).

init(Key) ->
  Promise = order:null(),
  Voted = order:null(),
  Accepted = na,
  acceptor(Key, Promise, Voted, Accepted).

acceptor(Key, Promise, Voted, Accepted) ->
  timer:sleep(random:uniform(?sleep)),
  receive
  {prepare, Proposer, Round} ->
    case order:gr(Round,Promise) of
      true ->
        Proposer ! {promise, Round, Voted, Accepted},
        acceptor(Key, Round, Voted, Accepted); %%Se actualiza la ultima promesa dada
      false ->
        %Proposer ! {sorry, Round}, %%{sorry, Promise}? informarle que prometimos no votar en la ronda solicitada por el proponente
        acceptor(Key, Promise, Voted, Accepted) %%Prometimos no votar en rondas menores a mi promise
    end;
    {accept, Proposer, Round, Proposal} ->
      io:format("me propuso ~w con round~w:~n", [Proposal, Round]),
      case order:goe(Round, Promise) of %%{Promise, Round}
        true ->
          Proposer ! {vote, Round}, %%aceptar la solicitud 
          case order:goe(Round, Voted) of %%Si aceptamos un valor en una ronda inferior
            true ->
              io:format("~w ultimo valor aceptado: ~w~n", [Key, Proposal]),
              acceptor(Key, Promise, Round, Proposal); %%aún se debe recordar el valor del número de votación más alto%
              
            false ->
              acceptor(Key, Promise, Voted, Accepted) %%Accepted = ultimo valor aceptado
          end;
        false ->
          %Proposer ! {sorry, Round},
          io:format("~w no pudo aceptar ~w~n", [Key, Proposal]),
          acceptor(Key, Promise, Voted, Accepted)
      end;
    stop ->
      unregister(Key),
      ok
  end.
