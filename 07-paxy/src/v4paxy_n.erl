-module(v4paxy_n).

-export([start/4, stops/1]).

-define(sleep, 2000).

%% Version eleccion de proposer activo

start(Seed, AcceptorMod, Acceptors, Proposers) ->
  lists:foreach(fun(A) -> register(A, apply(AcceptorMod, start, [A])) end, Acceptors),

  io:format("Orden que un proposer se vuelve activo ~w:~n", [max_name_sort(Proposers)]),

  %% creo lock encargado de la eleccion del proposer activo, le paso el nombre del proposer
  Locks = lists:map(fun({Name, _}) -> v4lock2:start(Name) end, Proposers),
  start_proposers(Proposers, Locks, Acceptors, Seed).

max_name_sort(Proposers) ->
  lists:sort(fun({Name1, _}, {Name2, _}) -> Name1 > Name2 end, Proposers).

start_proposers([], [], _, _) -> 1;
start_proposers([{Name, Value} | Proposers], [L | Locks], Acceptors, Seed) ->
  Acc = start_proposers(Proposers, Locks, Acceptors, Seed),

  %% le paso sus peers al lock, sacando a si mismo
  L ! {peers, lists:delete(L, Locks)},

  %% se agrega como parametro el Lock
  v4proposer:start(Name, Value, Acceptors, L, Seed+Acc),
  Acc + 1.


stops(Proposals) ->
  lists:foreach(fun(P) -> stop(P) end, Proposals).

stop(Name) ->
  case whereis(Name) of
    undefined ->
      ok;
    Pid ->
      Pid ! stop
  end.
