-module(paxy).

-export([start/1, stop/0]).

start(Seed) ->
  register(a, v1acceptor:start(a)),
  register(b, v1acceptor:start(b)),
  register(c, v1acceptor:start(c)),
  register(d, v1acceptor:start(d)),
  register(e, v1acceptor:start(e)),
  Acceptors = [a,b,c,d,e],
  v1proposer:start(kurtz, green, Acceptors, Seed+2),
  v1proposer:start(willard, red, Acceptors, Seed+1),
  v1proposer:start(kilgore, blue, Acceptors, Seed+3),
  true.

stop() ->
  stop(a),
  stop(b),
  stop(c),
  stop(d),
  stop(e).

stop(Name) ->
  case whereis(Name) of
    undefined ->
      ok;
    Pid ->
      Pid ! stop
  end.
