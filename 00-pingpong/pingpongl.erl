-module(pingpong).

-export([init_ping/1, init_pong/1, pp/0]).

init_ping(Dest) ->
  P = spawn(pingpongl, pp, []),
  Dest ! {ping, P}.

init_pong(Nombre) ->
  P = spawn(pingpongl, pp, []),
  register(Nombre, P).

pp() ->
  receive
    {ping, Recep} ->
      Recep ! {pong, self()},
      io:format("Pong enviado~n");
    {pong, Proc2} ->
      io:format("Pong: ~p~n", [Proc2])
  end.

%% se puede dividir en dos ping y pong, en vez de uno solo,
%% se puede agregar un kill para dejar de esperar un ping
%% un after en el ping para que no espere para siempre el pong
%%pong() ->
%%  receive
%%    {ping, Recep} ->
%%      Recep ! {pong, self()},
%%      io:format("Pong enviado~n")
%%    kill ->
%%      ok
%%  end.
%%
%%ping() ->
%%  receive
%%    {pong, Proc2} ->
%%      io:format("Pong: ~p~n", [Proc2])
%%  end.
%% kill(Node) -> {ping_wait_procress} ! kill
