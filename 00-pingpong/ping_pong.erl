-module(ping_pong).

-export([ping/1, ping_wait/0, start/0, kill/1]).


start() ->
    Pid = spawn(ping_pong, ping_wait, []),
    register(ping_wait_process, Pid).

ping_wait() ->
    receive
        {ping, From} ->
            io:format("Recibi un ping de~w~n", [From]),
            From ! pong;
        kill ->
            ok
    end.

ping(Node) ->
        {ping_wait_process, Node} ! {ping, self()},
        receive
            pong -> 
                io:format("Recibi un pong~n")
        after 2000 ->
                io:format("No recibi pong~n")
        end.

kill(Node) ->
    io:format("Se muere el proceso~n"),
    {ping_wait_process, Node} ! kill.
